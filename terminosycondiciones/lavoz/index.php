<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Terminos y Condiciones - La Voz</title>
	<?php include('../contenido/head2.php'); ?>
</head>    
<body>
	<div id="container">
		<?php include('../../assets/contenido/header/headerlavoz.php'); ?>   
		<div class="page-banner">         
			<div class="container">
				<h2>Terminos y Condiciones La Voz</h2>
			</div>
		</div>
 
	 <div class="section">
		 <div id="about-section">

             

			 <!-- filosofia -->
			 <div class="services-box">
				 <div class="container">
					 <div class="row">

					 	 <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Terminos y condiciones la voz" src="../../assets/images/legales/Legales-BMW-21-de-Enero.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>

	                     <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Terminos y condiciones la voz" src="../../assets/images/legales/Legales-Motorrad-21-de-Enero.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>

	                    


<!-- 
	                     <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Whatsapp" src="../../assets/images/legales/LegalesBMW_Mesa de trabajo 1.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>

	                     <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Whatsapp" src="../../assets/images/legales/LegalesMini_Mesa de trabajo 1.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>

	                     <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Whatsapp" src="../../assets/images/legales/Legales_Mini_22Octtubre.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>



	                      <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Whatsapp" src="../../assets/images/legales/Legales_Motorrad_22Oct.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>

	                      <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Whatsapp" src="../../assets/images/legales/LaVozLegales15deOct-BMW.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>

	                     <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Whatsapp" src="../../assets/images/legales/LaVozLegales15deOct-Motorrad.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>

	                     <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Whatsapp" src="../../assets/images/legales/Legales_08Oct_BMW_Mesa de trabajo 1.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>

	                     <div id="slider">
							 <div class="flexslider">
								 <ul class="slides">
								     <li>
								         <a href="#" target="_self"><img alt="Whatsapp" src="../../assets/images/legales/Legales_08Oct_Mini_Mesa de trabajo 1.jpg"></a>
								     </li> 
								 </ul>
								 <img class="shadow-image" alt="" src="../../assets/images/shadow.png">
							 </div>
	                     </div>
-->
	                     

	                     <br>

					 </div>
				 </div>
			 </div>

		 </div>
     </div>


			 <?php include_once('../../assets/contenido/footer2.php'); ?>