<!doctype html>
<html lang="es" class="no-js">
<head>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TMZ6Q4P');</script>
<!-- End Google Tag Manager -->

<title>Grupo FAME - Contacto</title>
   <?php include('contenido/head.php'); ?>

	<script src='https://www.google.com/recaptcha/api.js?render=6LerUI0UAAAAAKds34FWAActCHZn9bFawAK9LBBw'></script>

</head> 

<body>

	
	<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TMZ6Q4P"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->



	<div id="container">
		<?php include('../assets/contenido/header/headercontacto.php'); ?>   
		<div class="page-banner">         
			<div class="container">
				<h2>Contacto</h2>
			</div>
		</div>
 
	 <div class="contact-box">
		 <div class="container">

			 <div class="row">

				 <div class="col-md-6" align="center">
                     <div class="container">
					     <div class="col-md-12" >
							 <?php include('../assets/formularios/form.php'); ?>
                         </div>
                     </div>
                 </div>

                 <div class="col-md-3">
					 <div class="contact-information">
						 <h3>Información de Contacto</h3>
						 <ul class="contact-information-list">
							 <li><span><i class="fa fa-phone"></i>01800 670 8386 <strong>Call Center</strong></span></li>
							 <li><a href="#"><i class="fa fa-envelope"></i>contacto@grupofame.com</a></li>
                         </ul>
					 </div>
				 </div>

				 <div class="col-md-3">
					 <div class="contact-information">
						 <h3>Horario de Atención</h3>
						 <p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en Grupo FAME te escuchamos y atendemos de manera personalizada. </p>
						 <p class="work-time"><span>Lunes - Viernes</span> : 9:00 AM a 2:00 PM - 4:00 AM a 7:00 PM</p>
						 <p class="work-time"><span>Sábado</span> : 9:00 AM - 1:00 PM.</p>
                         <p class="work-time"><span>Domingo</span> <i>Cerrado</i></p>
					 </div>
				 </div>
			 </div>
		 </div>
	 </div>
 </div>
</body>

<?php include_once('../assets/contenido/footer.php'); ?>

 