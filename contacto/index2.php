<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Contacto</title>
	<?php include('contenido/head.php'); ?>

	<script src='https://www.google.com/recaptcha/api.js'></script>

	
</head> 

<body>
	<div id="container">
		<?php include('../assets/contenido/header/headercontacto.php'); ?>   
		<div class="page-banner">         
			<div class="container">
				<h2>Contacto</h2>
			</div>
		</div>
 
	 <div class="contact-box">
		 <div class="container">

			 <div class="row">

				 <div class="col-md-6" align="center">
                     <div class="container">
					     <div class="col-md-12" >
							 <?php include('../assets/formularios/form2.php'); ?>
                         </div>
                     </div>
                 </div>

                 <div class="col-md-3">
					 <div class="contact-information">
						 <h3>Información de Contacto</h3>
						 <ul class="contact-information-list">
							 <li><span><i class="fa fa-phone"></i>01800 670 8386 <strong>Call Center</strong></span></li>
							 <li><a href="#"><i class="fa fa-envelope"></i>contacto@grupofame.com</a></li>
                         </ul>
					 </div>
				 </div>

				 <div class="col-md-3">
					 <div class="contact-information">
						 <h3>Horario de Atención</h3>
						 <p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en Grupo FAME te escuchamos y atendemos de manera personalizada. </p>
						 <p class="work-time"><span>Lunes - Viernes</span> : 9:00 AM a 2:00 PM - 4:00 AM a 7:00 PM</p>
						 <p class="work-time"><span>Sábado</span> : 9:00 AM - 1:00 PM.</p>
                         <p class="work-time"><span>Domingo</span> <i>Cerrado</i></p>
					 </div>
				 </div>
			 </div>
		 </div>
	 </div>
 </div>
</body>

<?php include_once('../assets/contenido/footer.php'); ?>

 