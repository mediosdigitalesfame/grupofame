<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - WhatsApp</title>
	<?php include('contenido/head.php'); ?>
</head> 

<body>
	<div id="container">
		<?php include('../assets/contenido/header/headerwhats.php'); ?>   
		<div class="page-banner">         
			<div class="container">
				<h2>WhatsApp</h2>
			</div>
		</div>
 
	 <div class="contact-box">
		 <div class="container">

			 <div class="row">

				 <div class="col-md-12" align="center">
                     <div class="container">
					     <div class="col-md-6">
	                 <table class="table table-bordered">
                         <thead>
                             <tr height=14 style='height:14.0pt'>
                                 <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
                                 	 ESTADO DE MICHOACÁN<span style="mso-spacerun:yes">&nbsp;</span>
                                 </td>
                             </tr>

							  <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl115 style='height:14.0pt;border-top:none'>
							     	 AGENCIA
							     </td>
								 <td align="center" class=xl115 style='border-top:none;border-left:none'>
								 	 VENTAS<span style="mso-spacerun:yes">&nbsp;</span>
								 </td>
								 <td align="center" class=xl115 style='border-top:none;border-left:none'>
								 	 POSTVENTA<span style="mso-spacerun:yes">&nbsp;</span>
								 </td>
							 </tr>
                         </thead>
                         <tbody>
                         	 <tr height=14 style='height:14.0pt'>
	                             <td align="center" colspan=9 height=14 class=xl115 style='height:14.0pt'>
	                         	     MORELIA 
	                         	 </td>
	                         </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td  height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> HONDA ALTOZANO </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=525568849962&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5568849962</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=525554186443&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5554186443</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> HONDA MANANTIALES </font>
							     	 </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=525568849962&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5568849962</a>
							 </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=524432718558&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432718558</a>
							 </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black"> HONDA MONARCA </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=525568849962&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5568849962</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524431551131&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4431551131</a> <br>
							         <a href="https://api.whatsapp.com/send?phone=524432732795&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732795</a>
							     </td>
							 </tr>
							  <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> BMW </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     	<a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432270945&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432270945</a>
							     </td>
							 </tr>
							  <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> MINI </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=525513845300&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5513845300</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> BMW MOTORRAD </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     	<a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=52- - - - &text=Hola!%20Quiero%20más%20información!" target="_blank">&nbsp;</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> NISSAN ACUEDUCTO </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524431810975&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4431810975</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524431550451&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4431550451</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> NISSAN ALTOZANO </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'> - </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'> - </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> TOYOTA </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433250949</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732824&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732824</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> CHEVROLET </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433586914&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433586914</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732828&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732828</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> KIA </font>  
							     	</td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433253805&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433253805</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black">VOLKSWAGEN </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433250949</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732792&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732792</a>
							     </td>
							 </tr>
							  <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black">DAS WELT </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433250949</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola!%20Quiero%20más%20información!" target="_blank">  - </a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	<font size="2" color="black"> ISUZU </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433952597&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433952597</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524434101231&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4434101231</a>
							     </td>
							     
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	   <font size="2" color="black"> GMC, BUICK Y CADILLAC </font>   
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433250949</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432279923&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432279923</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
                                 <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
                                 	 URUAPAN<span style="mso-spacerun:yes">&nbsp;</span>
                                 </td>
                             </tr>
                             <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     <font size="2" color="black"> TOYOTA </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'> 
							     	 - 
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'> 
							         - 
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black"> HONDA </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=525568849962&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5568849962</a>
							 </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=524521058846&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4521058846</a>
							 </td>
							 </tr>
                              <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	<font size="2" color="black"> CHEVROLET </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433952597&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433952597</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524521000646&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4521000646</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> KIA </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524434712819&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4434712819</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> FIAT  </font>
							     	</td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=525568849962&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5568849962</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524521141002&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4521141002</a>
							     </td>
							 </tr>
                             <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> CHRYSLER </font>
							     	</td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=525568849962&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5568849962</a>
							 </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=524521141002&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4521141002</a>
							 </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> MITSUBISHI </font>
							     	 </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524521141002&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4521141002</a>
							     </td>
							 </tr>
							 
							 <tr height=14 style='height:14.0pt'>
                                 <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
                                 	   ZAMORA  
                                 </td>
                             </tr>
                             <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> KIA </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=5244327327898&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524434712818&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4434712818</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black"> CHEVROLET </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=524433952597&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433952597</a>
							 </td>
							     <td align="center" class=xl114 style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=523531081138&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 3531081138</a>
							 </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
                                 <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
                                 	   APATZINGAN 
                                 </td>
                             </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> CHEVROLET  </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433952597&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433952597</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524531104173&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4531104173</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
                                 <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
                                 	 LÁZARO CÁRDENAS<span style="mso-spacerun:yes">&nbsp;</span>
                                 </td>
                             </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black"> CHEVROLET </font>
							          </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=524433952597&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433952597</a>
							 </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=525591983328&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5591983328</a>
							 </td>
							 </tr>
							 
							 <tr height=14 style='height:14.0pt'>
                                 <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
                                 	 SAHUAYO<span style="mso-spacerun:yes">&nbsp;</span>
                                 </td>
                             </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							          <font size="2" color="black"> CHEVROLET </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433952597&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433952597</a>
							     </td>
							     <td align="center" class=xl112 style='border-top:none;border-left:none'>
							     	 &nbsp;
							     </td>
							 </tr>
                         </tbody>
                     </table>
                 </div>

				 <div class="col-md-6">
	                 <table class="table table-bordered" align="center" >
                         <thead>
	                         <tr height=14 style='height:14.0pt'>
							     <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
							     	 ESTADO DE QUERÉTARO<span style="mso-spacerun:yes">&nbsp;</span>
							     </td>
							 </tr>

							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl115 style='height:14.0pt;border-top:none'>
							         AGENCIA
							     </td>
								 <td align="center" class=xl115 style='border-top:none;border-left:none'>
								 	 VENTAS<span style="mso-spacerun:yes">&nbsp;</span>
								 </td>
								 <td align="center" class=xl115 style='border-top:none;border-left:none'>
								 	 POSTVENTA<span style="mso-spacerun:yes">&nbsp;</span>
								 </td>
							 </tr>
	                     </thead>
	                     <tbody>
	                     	 <tr height=14 style='height:14.0pt'>
							     <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
							     	 QUERÉTARO<span style="mso-spacerun:yes">&nbsp;</span>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
								 <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
								 <font size="2" color="black"> HONDA 5 DE FEBRERO </font>
								</td>
								 <td align="center" align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4425923862</a>
							     </td>
								 <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
								 <a href="https://api.whatsapp.com/send?phone=524433694057&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433694057</a>
								</td>
							 </tr>
							  <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black"> HONDA MARQUESA </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4425923862</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432026580&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432026580</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black"> HONDA CORREGIDORA &nbsp; &nbsp; </font>
							     </td>
							     
							     <td align="center" class=xl112 align="right" style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4425923862</a>
							     </td>
							     <td align="center" class=xl112 align="right" style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732431&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732431</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     <font size="2" color="black"> AUDI QUERÉTARO </font>
							 </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432192925&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432192925</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	<font size="2" color="black"> AUDI JURIQUILLA </font>
							     	</td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank"> +52 4432732789 </a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=52-&text=Hola!%20Quiero%20más%20información!" target="_blank"> - </a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>FIAT</td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4425923862</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433693818&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433693818</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	<font size="2" color="black"> CHRYSLER </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4425923862</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433693818&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433693818</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>  
							     	<font size="2" color="black"> MITSUBISHI</font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524432732789&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4432732789</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433693818&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433693818</a>
							     </td>
							 </tr>
							 
							  
							
							 <tr height=14 style='height:14.0pt'>
							     <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
							     	 SAN JUAN DEL RIO<span style="mso-spacerun:yes">&nbsp;</span>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	<font size="2" color="black"> HONDA &nbsp; &nbsp; &nbsp; </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4425923862</a>
							 </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=524433952597&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433952597</a>
							 </td>
							 </tr>
					     </tbody>
                         	 
                     </table>

                     <table class="table table-bordered">
					     <thead>
						     <tr height=14 style='height:14.0pt'>
						         <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
						         	CIUDAD Y ESTADO DE MÉXICO<span style="mso-spacerun:yes">&nbsp;</span>
						         </td>
						     </tr>

						     <tr height=14 style='height:14.0pt'>
						         <td align="center" height=14 class=xl115 style='height:14.0pt;border-top:none'>AGENCIA</td>
							     <td align="center" class=xl115 style='border-top:none;border-left:none'>VENTAS<span
								  style="mso-spacerun:yes">&nbsp;</span></td>
							     <td align="center" class=xl115 style='border-top:none;border-left:none'>POSTVENTA<span
								  style="mso-spacerun:yes">&nbsp;</span></td>
						     </tr>
					     </thead>
	                     <tbody>
	                     	 <tr height=14 style='height:14.0pt'>
							     <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
							     	 CIUDAD DE MÉXICO<span style="mso-spacerun:yes">&nbsp;</span>
							     </td>
							 </tr>
						     
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black"> HONDA &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433250949 &nbsp; &nbsp;</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=525513537530&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5513537530</a>
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
							         <font size="2" color="black"> TOYOTA  </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433250949</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=525532325380&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5532325380 </a><br>
							         <a href="https://api.whatsapp.com/send?phone=525529221489&text=Hola!%20Quiero%20más%20información!" target="_blank">
							     +52 5529221489</a> <br>
							     <a href="https://api.whatsapp.com/send?phone=525539937307&text=Hola!%20Quiero%20más%20información!" target="_blank">
							     +52 5539937307</a>
							     </td>
							 </tr>
							 
							 <tr height=14 style='height:14.0pt'>
							     <td  height=14 class=xl112 style='height:14.0pt;border-top:none'>
							     	 <font size="2" color="black"> KIA PEDREGAL </font>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							         <a href="https://api.whatsapp.com/send?phone=524431810975&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4431810975</a>
							     </td>
							     <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
							     <a href="https://api.whatsapp.com/send?phone=525514511270&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5514511270</a>
							 </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
							     <td colspan=9 height=14 class=xl115 style='height:14.0pt'>
							     	 ESTADO DE MÉXICO 
							     </td>
							 </tr>
							 <tr height=14 style='height:14.0pt'>
						         <td height=14 class=xl112 style='height:14.0pt;border-top:none'>
						         	 <font size="2" color="black"> HONDA ATIZAPÁN </font> &nbsp; &nbsp; &nbsp; &nbsp;
						         	</td>
						         <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
						             <a href="https://api.whatsapp.com/send?phone=524433250949&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 4433250949</a>
						         </td>
						         <td align="center" class=xl112 align=right style='border-top:none;border-left:none'>
						             <a href="https://api.whatsapp.com/send?phone=525513338485&text=Hola!%20Quiero%20más%20información!" target="_blank">+52 5513338485</a>
						         </td>
						     </tr>
						 </tbody>
	                 </table>
                 </div>
                     </div>
                 </div>

                 

				 
			 </div>
		 </div>
	 </div>
 </div>
</body>

<?php include_once('../assets/contenido/footer.php'); ?>

 