 
	<title>Grupo FAME</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	<meta name="Title" content="Grupo FAME">
	<meta name="description" content="Tu mejor opción para adquirir un vehículo. Somos el grupo automotriz más grande de México y Latinoamérica, con más de 1600 unidades en inventario y tenemos presencia en 4 Estados de la República, 11 Ciudades y te ofrecemos 19 distintas marcas. Piensa en auto, piensa en FAME.">
    <meta name="keywords" content="Grupo Fame, autos 2017, Audi queretaro, audi fame, BMW, bmw fame, bmw morelia, Mini, mini fame, mini morelia, Honda, honda fame, honda michoacan, honda morelia, honda df, honda mexico, honda queretaro, honda estado de mexico, Toyota, toyota fame, toyota morelia, toyota perisur, toyota df, Mitsubishi, mitsubishi fame, mitsubishi uruapan, mitsubishi queretaro, Isuzu, isuzu fame, fame camiones, isuzu morelia, GMC, gmc fame, gmc uruapan, gmc morelia, gmc manantiales, Buick, buick morelia, buick uruapan, buick manantiales, buick fame, Cadillac, cadillac fame, cadillac morelia, cadillac uruapan, fame manantiales, fame manantiales morelia, fame manantiales uruapan, Nissan, nissan fame, nissan acueducto, nissan morelia, Suzuki, suzuki fame, suzuki santa fe, suzuki pedregal, Chevrolet, chevrolet fame, chevrolet michoacan, chevrolet morelia, chevrolet uruapan, chevrolet apatzingan, chevrolet lazaro cardenas, chevrolet zamora, chevrolet sahuayo, Chrysler, chrysler fame, chrysler uruapan, chrysler queretaro, Dodge, dodge fame, dodge uruapan, dodge queretaro, Jeep, jeep fame, jeep uruapan, jeep queretaro, Ram, ram fame, ram uruapan, ram queretaro, Peugeot, peugeot fame, peugeot morelia, peugeot michoacan, fame morelia, fame queretaro, fame uruapan, fame michoacan, fame altozano, Morelia, Querétaro, México, seminuevos morelia, seminuevos uruapan, seminuevos queretaro, seminuevos fame, seminuevos en morelia, Autos, Nuevos, Seminuevos, Agencias, Servicio, Taller, Hojalatería, Pintura, kia mexico, kia pedregal, kia, kia motors, kia motors mexico, kia motors pedregal, sorento, forte, sportage, vehiculos, agencia kia">
    <meta name="author" content="Grupo Fame División Automotriz">
    
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,400,700,600,300' rel='stylesheet' type='text/css'>
	<script src="https://code.jquery.com/jquery-3.2.1.js"></script>

	<link rel="stylesheet" href="../assets/css/bootstrap.css" type="text/css" media="screen">
	<link rel="stylesheet" type="text/css" href="../assets/css/magnific-popup.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../assets/css/owl.carousel.css" media="screen">
    <link rel="stylesheet" type="text/css" href="../assets/css/owl.theme.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../assets/css/jquery.bxslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../assets/css/font-awesome.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="../assets/css/style.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../assets/css/flexslider.css" media="screen">
	<link rel="stylesheet" type="text/css" href="../assets/css/responsive.css" media="screen">
    <link rel="stylesheet" type="text/css" href="../assets/css/fullwidth.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="../assets/css/settings.css" media="screen" />
    <link rel="icon" type="image/png" href="../assets/images/favicon.png" />

    <!--	ANALYTICS GRUPO FAME	-->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-47663622-1', 'auto');
  ga('send', 'pageview');

</script>

<!--	FIN ANALYTICS	--> 


<style>
         <!-- 
         table
	{mso-displayed-decimal-separator:"\.";
	mso-displayed-thousand-separator:"\,";}
         @page
	{margin:.75in .7in .75in .7in;
	mso-header-margin:.3in;
	mso-footer-margin:.3in;}
         .style0
	{mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	white-space:nowrap;
	mso-rotate:0;
	mso-background-source:auto;
	mso-pattern:auto;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	border:none;
	mso-protection:locked visible;
	mso-style-name:Normal;
	mso-style-id:0;}
         td
	{mso-style-parent:style0;
	padding-top:1px;
	padding-right:1px;
	padding-left:1px;
	mso-ignore:padding;
	color:black;
	font-size:11.0pt;
	font-weight:400;
	font-style:normal;
	text-decoration:none;
	font-family:Calibri, sans-serif;
	mso-font-charset:0;
	mso-number-format:General;
	text-align:general;
	vertical-align:bottom;
	border:none;
	mso-background-source:auto;
	mso-pattern:auto;
	mso-protection:locked visible;
	white-space:nowrap;
	mso-rotate:0;}
		 .xl112
			{mso-style-parent:style0;
			font-family:"Century Gothic", sans-serif;
			mso-font-charset:0;
			border:.5pt solid windowtext;}
		 .xl113
			{mso-style-parent:style0;
			font-family:"Century Gothic", sans-serif;
			mso-font-charset:0;}
		 .xl114
			{mso-style-parent:style0;
			font-family:"Century Gothic", sans-serif;
			mso-font-charset:0;
			text-align:right;
			border:.5pt solid windowtext;}
		 .xl115
			{mso-style-parent:style0;
			color:white;
			font-family:"Century Gothic", sans-serif;
			mso-font-charset:0;
			text-align:center;
			border:.5pt solid windowtext;
			background:#1F497D;
			mso-pattern:black none;}
		 .xl116
			{mso-style-parent:style0;
			color:white;
			font-family:"Century Gothic", sans-serif;
			mso-font-charset:0;
			text-align:center;
			vertical-align:middle;
			border-top:none;
			border-right:none;
			border-bottom:.5pt solid windowtext;
			border-left:none;
			background:#1F497D;
			mso-pattern:black none;}
         -->
         table {
             width: 100%;
             }
     </style>
 