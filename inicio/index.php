<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		<?php include_once('../assets/contenido/header/header.php'); ?>
		<div id="content">
			<?php include('../assets/contenido/bienvenida.php'); ?>
			<?php include('../assets/contenido/banners.php'); ?>
			<?php include('../assets/contenido/agencias.php'); ?>
			<?php include('../assets/contenido/video.php'); ?> 
			<?php include('../assets/contenido/citas.php'); ?>  
		</div>
		<?php include_once('../assets/contenido/footer.php'); ?>
	</div>
</body>
</html>

