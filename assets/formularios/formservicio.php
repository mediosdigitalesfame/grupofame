<?php
	 if (isset($_POST['boton'])) 
         {
			 if($_POST['nombre'] == '') 
			     {
				     $errors[1] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-user"> </i>
				         </span><span class="error">Ingrese su nombre.</span> 
				     </div>';
				 } 
			 else if($_POST['email'] == '' or !preg_match("/^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/",$_POST['email']))
				 {
				     $errors[2] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-envelope"> </i>
				         </span><span class="error">Ingrese un e-Mail correcto.</span> 
				     </div>';
				 } 
			 else if(empty($_POST['telefono'])) 
				 {
				     $errors[3] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-phone"> </i>
				         </span><span class="error">Ingrese un Número Telefonico.</span> 
				     </div>';
				 } 
			 
			 else if(empty($_POST['mensaje'] )) 
				 {
				     $errors[4] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-pencil"> </i>
				         </span><span class="error">Ingrese un mensaje.</span> 
				     </div>';
				 } 
		     else if($_POST['marca'] == '0') 
			     {
				     $errors[5] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-user"> </i>
				         </span><span class="error">Selecciona una Marca.</span> 
				     </div>';
				 } 

		     else if($_POST['modelo'] == '0') 
			     {
				     $errors[6] = '
				     <div class="alert alert-danger" role="alert" > 
				         <span><i class="fa fa-user"> </i>
				         </span><span class="error">Elige un Modelo.</span> 
				     </div>';
				 } 
			  
			 else 
				 {   
				 	  $dest = "contacto@grupofame.com, formas@grupofame.com" ;

					 $nombre = $_POST['nombre'];
					  
					 $email = $_POST['email'];
					 $telefono = $_POST['telefono'];
					 $asunto_cte = "Formulario de Contacto Grupo FAME Cita de Servicio";
					 $asunto = "Formulario de Contacto Grupo FAME Cita de Servicio";
					 $cuerpo = $_POST['mensaje'];
					 $cuerpo_mensaje = '

						 <html>

							 <head>
								 <title>Mail from '. $nombre .'</title>
							 </head>

							 <body>
								 <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
								     <TH COLSPAN=2 >'. $servicio .'</TH> 
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Telefono:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $telefono .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Telefono:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $marca .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Telefono:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $modelo .'</td>
									 </tr>
									  
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
									 </tr>
								 </table>
							 </body>
						 </html>';

						 $cuerpo_cte = '

						 <html>

							 <head>
								 <title>Mail from '. $nombre .'</title>
							 </head>

							 <body>
								 <table style="width: 500px; font-family: arial; font-size: 14px;" border="1">
								     <tr style="height: 32px;"><TH COLSPAN=2 >CONFIRMAMOS LA RECEPCION DE TUS DATOS</TH> </tr>
<tr style="height: 32px;"><TH COLSPAN=2 >'. $servicio .'</TH> </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Nombre:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $nombre .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Correo:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $email .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Telefono:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $telefono .'</td>
									 </tr>
									 <tr style="height: 32px;">
										 <th align="right" style="width:150px; padding-right:5px;">Mensaje:</th>
										 <td align="left" style="padding-left:5px; line-height: 20px;">'. $cuerpo .'</td>
									 </tr>
								 </table>
							 </body>
						 </html>';
					  

					 $headers  = 'MIME-Version: 1.0' . "\r\n";
					 $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
					 $headers .= 'From: ' . $email . "\r\n";

					 $headers2  = 'MIME-Version: 1.0' . "\r\n";
$headers2 .= 'Content-type: text/html; charset=utf-8' . "\r\n";
$headers2 .= "From: Grupo FAME <contacto@grupofame.com>\r\n";
					            
					 if(mail($dest,$asunto,$cuerpo_mensaje,$headers))
					     {
					         $result = '
					             <div class="alert alert-success" role="alert" id="success_message">
         	                         <strong> Mensaje Enviado <i class="fa fa-thumbs-up"></i> </strong> <br> 
         	                         Gracias por Contactarnos, <br> En breve uno de nuestros asesores te atendera.
                                 </div>';
                                  mail($email,$asunto_cte,$cuerpo_cte,$headers2);
						     $_POST['nombre'] = '';
						     $_POST['email'] = '';
						     $_POST['telefono'] = '';
						     $_POST['mensaje'] = '';
						     $_POST['servicio'] = '';
							 
						      
				         } 
				     else 
				         {
				             $result = '
                                 <div class="alert alert-warning" role="alert" id="failure_message">
         	                         Mensaje NO Enviado <i class="fa fa-thumbs-down"></i> <br>
         	                         Intenta Nuevamente.
                                 </div>
				             ';
				         }
				 }
		 }
 ?>

 <div class="container">
     <form class="well2 form-horizontal" action='' method="post"  id="contact_form" action=''>
 
         <fieldset>

         <h2><legend2>¡Agenda Tu Cita de Servicio!</legend2></h2>
<labelform>
		 <!-- Input NOMBRE-->
		 <div class="form-group">  
		     <div class="col-md-12 inputGroupContainer">
			     <div class="input-group">
			         <span class="input-group-addon2"><i class="fa fa-user"></i></span>
			         <input name="nombre" id="nombre" placeholder="Nombre Completo" class="form-control" type="text" value='<?php if(isset($_POST['nombre'])){ echo $_POST['nombre']; } ?>' >
			     </div>
			 </div>
		 </div>

		 <!-- Input MAIL-->
		 <div class="form-group"> 
			 <div class="col-md-12 inputGroupContainer">
			     <div class="input-group">
			         <span class="input-group-addon2"><i class="fa fa-envelope"></i></span>
			         <input name="email" id="email" placeholder="Correo Electrónico" class="form-control" type="text" value='<?php if(isset($_POST['email'])){ echo $_POST['email']; } ?>'>
			     </div>
			 </div>
		 </div>

		 <!-- Input TELEFONO-->
		 <div class="form-group">  
		     <div class="col-md-12 inputGroupContainer">
		         <div class="input-group">
		             <span class="input-group-addon2"><i class="fa fa-phone"></i></span>
		             <input name="telefono" id="telefono" placeholder="Teléfono" class="form-control" type="text" value='<?php if(isset($_POST['telefono'])){ echo $_POST['telefono']; } ?>'>
		         </div>
		     </div>
		 </div>

		 <!-- Input MARCA-->
		 <div class="form-group">  
		     <div class="col-md-12 inputGroupContainer">
		         <div class="input-group">
		             <span class="input-group-addon2"><i class="fa fa-car"></i></span>
		             <select name="marca" id="marca" class="form-control">
                        <option value="0">Selecciona una Marca</option>
                        <option value="1">Chevrolet</option>
                        <option value="2">Isuzu</option>
                        <option value="3">Honda</option>
                        <option value="4">Mitsubishi</option>
                        <option value="5">BMW</option>
                        <option value="6">Volkswagen</option>
                        <option value="7">BMW Motorrad</option>
                        <option value="8">Chrysler</option>
                        <option value="9">Fiat</option>
                        <option value="10">Kia</option>
                        <option value="11">Toyota</option>
                        <option value="12">Nissan</option>
                        <option value="13">GMC</option>
                        <option value="14">Buick</option>
                    </select>
		         </div>
		     </div>
		 </div>

		 <!-- Input MODELO-->
		 <div class="form-group">  
		     <div class="col-md-12 inputGroupContainer">
		         <div class="input-group">
		             <span class="input-group-addon2"><i class="fa fa-car"></i></span>
		             <select name="modelo" id="modelo" class="form-control">
                        <option value="0">Modelos</option>
                         
                    </select>
		         </div>
		     </div>
		 </div>
 
		 <!-- Text area --> 
		 <div class="form-group">
		     <div class="col-md-12 inputGroupContainer">
		         <div class="input-group">
		             <span class="input-group-addon2"><i class="fa fa-pencil"></i></span>
		             <textarea maxlength="10000" rows="5" class="form-control" placeholder="Escribe tu Mensaje" id="mensaje" name="mensaje"><?php if(isset($_POST['mensaje'])) { echo $_POST['mensaje']; } ?></textarea>
		         </div>
		     </div>
		 </div>
</labelform>
         <!-- Mensaje ENVIADO -->
         <?php if(isset($result)) { echo $result; } ?>
         

         <!-- Boton ENVIAR -->
         <div class="form-group">
             <label class="col-md-4 control-label"></label>
             <div class="col-md-4">
                 <button name='boton' type="submit" class="btn btn-succes" >ENVIAR <span class="fa fa-paper-plane"></span></button>
             </div>
         </div>

         <div class="form-group">
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[1]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[2]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[3]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[4]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[5]; } ?>
		     </div>
		     <div class="col-md-12 ">
         	         <?php if(isset($errors)){ echo $errors[6]; } ?>
		     </div>
		      
		 </div>

         </fieldset>
     </form>


 
<script language="javascript">
$(document).ready(function(){
    $("#marca").on('change', function () {
        $("#marca option:selected").each(function () {
            elegido=$(this).val();
            $.post("modelos.php", { elegido: elegido }, function(data){
                $("#modelo").html(data);
            });			
        });
   });
});
</script>

</head>
</div><!-- /.container -->

