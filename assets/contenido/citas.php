 <!-- CITAS -->
 <div class="services-box">
 	<div class="container">
 		<div class="row">

 			<div class="col-md-4" >
 				<div class="services-post">
 					<a class="list-menu services-icon1" href="../manejo"><i class="fa fa-check-circle"></i></a>
 					<div class="services-post-content">
 						<h4>CITA DE MANEJO</h4>
 						<p>Entra a éste enlace para agendar la cita de manejo en la agencia de tu preferencia.</p>
 					</div>
 				</div>
 			</div>

 			<div class="col-md-4" >
 				<div class="services-post">
 					<a class="services-icon2" href="http://notifame.com/" target="_blank"><i class="fa fa-book"></i></a>
 					<div class="services-post-content">
 						<h4>Revista NOTIFAME</h4>
 						<p>Consulta nuestra revista en formato digital, te ofrecemos promociones, información práctica de autos, eventos y lanzamientos.</p>
 					</div>
 				</div>
 			</div>

 			<div class="col-md-4" >
 				<div class="services-post">
 					<a class="list-menu services-icon3" href="../servicio"><i class="fa fa-gears"></i></a>
 					<div class="services-post-content">
 						<h4>CITA DE SERVICIO</h4>
 						<p>Entra aquí para fijar una cita de servicio para tu auto en cualquiera de nuestras agencias FAME</p>
 					</div>
 				</div>
 			</div>

 		</div>
 	</div>
 	<img class="shadow-image" alt="" src="../assets/images/shadow.png">
 </div>           
 <!--FIN CITAS-->         
 
 <!-- ENLACES -->
 <div class="services-box">
 	<div class="container">
 		<div class="row">

 			<div class="col-md-6">
 				<div class="services-post">
 					<a class="list-menu services-icon1" href="../blog"><i class="fa fa-newspaper-o"></i></a>
 					<div class="services-post-content">
 						<h4>#AutoFAME Blog</h4>
 						<p>Entérate de todas las novedades de la industria automotriz, aplica los tips y recomendaciones que tenemos para el máximo cuidado de tu automóvil.</p>
 					</div>
 				</div>
 			</div>

 			<div class="col-md-6">
 				<div class="services-post">
 					<a class="services-icon2" href="https://www.fameseminuevos.com/index.php/agencia/agencia/1" target="_blank"><i class="fa fa-car"></i></a>
 					<div class="services-post-content">
 						<h4>FAME SEMINUEVOS</h4>
 						<p>Entra a nuestra sección para encontrar tu próxima unidad seminueva, certificada y garantizada por nuestros asesores de servicio calificados..</p>
 					</div>
 				</div>
 			</div>

 		</div>
 	</div>
 	<img class="shadow-image" alt="" src="../assets/images/shadow.png">
 </div>              
 <!--FIN ENLACES--> 