	 <footer>
	 	<div class="footer-line">
	 		<div class="container">
	 			<p>
	 				2018 Grupo FAME | 
	 				<i class="fa fa-user"></i>
	 				<a class="list-menu" href="../avisodeprivacidad">
	 					<font color="#FFFFFF">
	 						<strong>  Aviso de privacidad</strong>
	 					</font>
	 				</a> |  <i align="right" class="fa fa-check"> </i>

	 				<a class="list-menu" href="../terminosycondiciones">
	 					<font align="right" color="#FFFFFF">
	 						<strong>  Terminos y Condiciones</strong>
	 					</font>
	 				</a>  |  <i align="right" class="fa fa-lock"> </i>

	 				<a class="list-menu" href="../formatoarco">
	 					<font align="right" color="#FFFFFF">
	 						<strong>  Formato ARCO </strong> 
	 					</font>
	 				</a> 
	 			</p>
	 			<a class="go-top" href="#"></a>
	 		</div>
	 	</div>
	 </footer>

	</div>

	<!-- <script src="https://use.fontawesome.com/b93ad5b4e6.js"></script> -->
	<script type="text/javascript" src="../assets/js/jquery.min.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.migrate.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="../assets/js/bootstrap.js"></script>
	<script type="text/javascript" src="../assets/js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.imagesloaded.min.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="../assets/js/jquery.flexslider.js"></script>
	<script type="text/javascript" src="../assets/js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="../assets/js/plugins-scroll.js"></script>
	<script type="text/javascript" src="../assets/js/script.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="../assets/js/gmap3.min.js"></script>
	<!-- <script src="https://maps.googleapis.com/maps/api/js"></script> -->
	<script type="text/javascript" src="../assets/js/script.js"></script>

	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->


