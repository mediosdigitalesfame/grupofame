<!-- VEHICULOS  HONDA -->
	<div id="content">

			<div class="latest-post">
				<div class="title-section">
					<h1>Nuestros <span> Vehículos</span></h1>
					<p>Navega a través de la sección para seleccionar el auto de tu preferencia</p>
				</div>
                
				<div id="owl-demo" class="owl-carousel owl-theme">

					  <div class="item news-item">
	                   	 <center>
 
							 <a href="pdfs/brv2018.pdf" target="_blank"><img alt="BRV 2018" src="images/autos/brv2018.png"></a>
 
							  <h2> <strong> <em> BR-V <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/brv2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/typer.pdf" target="_blank"><img alt="Type r 2017" src="images/autos/typer.png"></a>
							  <h2> <strong> <em> HONDA <sup>&reg;</sup> <font color="#DD1707">TYPE R</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/typer.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/typer.pdf" target="_blank"><img alt="Type r 2017" src="images/autos/accord2018.png"></a>
							  <h2> <strong> <em> ACCORD <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/accord2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/civic2018.pdf" target="_blank"><img alt="Civic Coupe 2018" src="images/autos/civic2018.png"></a>
							  <h2> <strong> <em> CIVIC <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/civic2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/civiccoupe2018.pdf" target="_blank"><img alt="Civic Coupe 2018" src="images/autos/civiccoupe2018.png"></a>
							  <h2> <strong> <em> CIVIC COUPE <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/civiccoupe2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/city2018.pdf" target="_blank"><img alt="City 2018" src="images/autos/city2018.png"></a>
							  <h2> <strong> <em> CITY <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/city2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/fit2018.pdf" target="_blank"><img alt="Fit 2018" src="images/autos/fit2018.png"></a>
							  <h2> <strong> <em> FIT <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/fit2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/hrv2018.pdf" target="_blank"><img alt="HR-V 2018" src="images/autos/hrv2018.png"></a>
							  <h2> <strong> <em> HR-V <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/hrv2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					 <div class="item news-item">
	                   	 <center>
							 <a href="pdfs/odyssey2018.pdf" target="_blank"><img alt="Odyssey 2018" src="images/autos/odyssey2018.png"></a>
							  <h2> <strong> <em> ODYSSEY <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/odyssey2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>


 					 <div class="item news-item">
	                   	 <center>
							 <a href="crv" target="_blank"><img alt="CR-V 2018" src="images/autos/crv2018.png"></a>
							  <h2> <strong> <em> CR-V <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/crv2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>

 					  <div class="item news-item">
	                   	 <center>
							 <a href="crv" target="_blank"><img alt="CR-V 2018" src="images/autos/pilot2018.png"></a>
							  <h2> <strong> <em> PILOT <sup>&reg;</sup> <font color="#DD1707">2018</font> </em> </strong></h2> </font>  
							 <a class="read-more" href="pdfs/pilot2018.pdf" target="_blank">Características <i class="fa fa-arrow-right"></i></a>
	                     </center>
 					 </div>
</div>
			</div>
<!-- FIN VEHICULOS HONDA -->