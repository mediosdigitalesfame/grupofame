	 <footer>
		 <div class="footer-line">
			 <div class="container">
				 <p>
				 	 2018 Grupo FAME | 
				 	 <i class="fa fa-user"> </i><a class="list-menu" href="avisoformulario.php"><font color="#FFFFFF"><strong>  Aviso de privacidad</strong></font></a> |  <i align="right" class="fa fa-check"> </i><a class="list-menu" href="terminosycondiciones.php"><font align="right" color="#FFFFFF"><strong>  Terminos y Condiciones</strong></font></a>
				 </p>
				 <a class="go-top" href="#"></a>
			 </div>
		 </div>
	 </footer>
 
 </div>

    <script src="https://use.fontawesome.com/b93ad5b4e6.js"></script>
	<script type="text/javascript" src="js/jquery.min.js"></script>
	<script type="text/javascript" src="js/jquery.migrate.js"></script>
	<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
	<script type="text/javascript" src="js/bootstrap.js"></script>
	<script type="text/javascript" src="js/owl.carousel.min.js"></script>
	<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
  	<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
	<script type="text/javascript" src="js/jquery.flexslider.js"></script>
	<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
	<script type="text/javascript" src="js/plugins-scroll.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
	<script type="text/javascript" src="js/gmap3.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js"></script>
 
	<!--[if lt IE 9]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->