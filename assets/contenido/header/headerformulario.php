
<!-- Header -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							<span><i class="fa fa-phone"></i>01800 670 8386</span>
							<span><i class="fa fa-envelope-o"></i>contacto@grupofame.com</span>
							<span> 
						<a href="../assets/images/pagarencaja.jpg" target="_blank" >
						<i class="fa fa-exclamation-circle"></i>Aviso pago 
					</a>
					</span>
						</p>
						<ul class="social-icons">
                        <li><a class="whatsapp" href="../whatsapp/" target="_self"><i class="fa fa-whatsapp"></i></a></li>
							<li><a class="facebook" href="https://www.facebook.com/grupofame" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/grupofame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                           <li>
						<a class="instagram" href="https://www.instagram.com/grupofame" target="_blank">
							<i class="fa fa-instagram" aria-hidden="true"></i> 
						</a>
					</li>
						</ul>
					</div>
				</div>
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="index.php"><img alt="" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li id="inicio" class=""><a href="index.php" class="list-menu">INICIO</a></li>

							 
							
                            
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->