<!-- Header -->
		<header class="clearfix">
			<!-- Static navbar -->
			<div class="navbar navbar-default navbar-fixed-top">
				<div class="top-line">
					<div class="container">
						<p>
							<span><i class="fa fa-phone"></i>01800 670 8386</span>
							<span><i class="fa fa-envelope-o"></i>contacto@grupofame.com</span>
							<span> 
						<a href="../assets/images/pagarencaja.jpg" target="_blank" >
						<i class="fa fa-exclamation-circle"></i>Aviso pago 
					</a>
					</span>
						</p>
						<ul class="social-icons">
                        <li><a class="whatsapp" href="../whatsapp/" target="_self"><i class="fa fa-whatsapp"></i></a></li>
							<li><a class="facebook" href="https://www.facebook.com/grupofame" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li><a class="twitter" href="https://twitter.com/grupofame" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a class="youtube" href="https://www.youtube.com/user/GrupoFameAutos" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                            <li>
						<a class="instagram" href="https://www.instagram.com/grupofame" target="_blank">
							<i class="fa fa-instagram" aria-hidden="true"></i> 
						</a>
					</li>
						</ul>
					</div>
				</div>
				<div class="container">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#"><img alt="" src="images/logo.png"></a>
					</div>
					<div class="navbar-collapse collapse">
						<ul class="nav navbar-nav navbar-right">

							<li id="index" class=""><a href="inicio" class="list-menu">INICIO</a></li>
							<li id="nosotros" class=""><a href="nosotros" class="list-menu">NOSOTROS</a></li>
							<li id="blog" class=""><a href="blog" class="list-menu">BLOG</a></li>

							 
							<li id="agencias" class="" class="drop"><a href="agencias" class="list-menu" >AGENCIAS</a>
								<ul class="drop-down">
									<li class=""><a class="list-menu" href="audi">AUDI</a></li>
									<li class=""><a class="list-menu" href="talisman">BMW·MINI</a></li>
									<li class=""><a class="list-menu" href="motorrad">BMW·Motorrad</a></li>
									<li class=""><a class="list-menu" href="gmc">GMC·BUICK·CADILLAC</a></li>
									<li class=""><a class="list-menu" href="chevrolet">CHEVROLET</a></li>														      <li class=""><a class="list-menu" href="chrysler">CHRYSLER·DODGE·JEEP·RAM</a></li>                                   
                                    <li class=""><a class="list-menu" href="honda">HONDA</a></li>
                                    <li class=""><a class="list-menu" href="isuzu">ISUZU</a></li>
                                    <li class=""><a class="list-menu" href="kia">KIA MOTORS</a></li>
									<li class=""><a class="list-menu" href="mitsubishi">MITSUBISHI</a></li>
                                    <li class=""><a class="list-menu" href="fiat">FIAT</a></li>
                                    <li class=""><a class="list-menu" href="nissan">NISSAN</a></li>
                               		<li class=""><a class="list-menu" href="toyota">TOYOTA</a></li>
                                    <li class=""><a class="list-menu" href="daswelt">DAS WELTAUTO</a></li>
                                    <li class=""><a class="list-menu"  href="vw">VOLKSWAGEN</a></li>
                                 </ul>
							</li>
							<li ><a href="https://www.fameseminuevos.com/index.php/agencia/agencia/1" target="_self">SEMINUEVOS</a></li>
							<li id="contacto" class=""><a href="contactohonda" class="list-menu">CONTACTO</a>
                                <ul class="drop-down">
									<li class=""><a class="list-menu" href="contactohonalto">Honda Altozano</a></li>
									<li class=""><a class="list-menu" href="contactohonatiz">Honda Atizapan</a></li>
									<li class=""><a class="list-menu" href="contactohoncorre">Honda Corregidora</a></li>
									<li class=""><a class="list-menu" href="contactohonamanan">Honda Manantiales</a></li>
									<li class=""><a class="list-menu" href="contactofamemondf">Honda Monarca DF</a></li>	
                                    <li class=""><a class="list-menu" href="contactofamemonmor">Honda Monarca Morelia</a></li>   
                                    <li class=""><a class="list-menu" href="contactofamemarq">Honda Marquesa</a></li>
                                    <li class=""><a class="list-menu" href="contactohdu">Honda Uruapan</a></li>
                                    <li class=""><a class="list-menu" href="contactohonsanju">Honda San Juan del Rio</a></li>
									<li class=""><a class="list-menu" href="contactohon5feb">Honda 5 de Febrero</a></li>
                                 </ul>
                            </li>
                            <li id="whatsap" class=""><a href="whatsapp" class="list-menu">WHATSAPP</a></li>
                            
						</ul>
					</div>
				</div>
			</div>
		</header>
		<!-- End Header -->