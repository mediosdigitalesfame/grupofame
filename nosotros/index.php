<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Nosotros</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		<?php include('../assets/contenido/header/headernosotros.php'); ?>   
		<div class="page-banner">         
			<div class="container">
				<h2>Nosotros</h2>
			</div>
		</div>
		<div class="section">
			<div id="about-section">
				<div class="welcome-box">
					<div class="container">
						<h1><span>Nosotros</span></h1><br>
						<p><strong>Grupo FAME</strong> es actualmente uno de los principales comercializadores automotrices en México y Latinoamérica, gracias a la implantación de procesos y sistemas bien definidos dentro de la empresa, las cuales realizan un trabajo corporativo integral y permiten enfrentar los retos que se presentan día a día y así alcanza los estándares con los que Grupo FAME cuenta actualmente para lograr la satisfacción de cada uno de nuestros clientes.<br><br>

						Sin embargo, esto no sería posible sin el intenso trabajo de nuestro equipo integrado por profesionales altamente capacitados y comprometidos en sus diversas áreas, que con esfuerzo y dedicación ejemplar en conjunto, buscan lograr una mayor eficiencia adaptándose a las nuevas necesidades del mercado incluyendo las más novedosas aplicaciones tecnológicas.</p>
					</div>
				</div>
				<!-- filosofia -->
				<div class="services-box">
					<div class="container">
						<div class="row">
							<div class="col-md-4">
								<div class="services-post">
									<a class="services-icon1" href="#"><i class="fa fa-briefcase"></i></a>
									<div class="services-post-content">
										<h4>Misión</h4>
										<p>Obtener la máxima satisfacción del cliente con sentido humano en busca de una alta rentabilidad</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services-post">
									<a class="services-icon2" href="#"><i class="fa fa-arrow-up"></i></a>
									<div class="services-post-content">
										<h4>Visión</h4>
										<p>Convertirnos en el principal grupo comercializador automotriz del país en todos los segmentos de mercado y niveles de ingreso a los que sirve esta industria con el espíritu de satisfacer las necesidades de sus clientes teniendo una estructura humana operativa, sólida y eficaz.</p>
									</div>
								</div>
							</div>
							<div class="col-md-4">
								<div class="services-post">
									<a class="services-icon3" href="#"><i class="fa fa-users"></i></a>
									<div class="services-post-content">
										<h4>Valores</h4>
										<p>Se basan en practicar la honradez y la ética en todas nuestras acciones con responsabilidad, lealtad y actitud positiva ante todos los retos, trabajamos con austeridad y sencillez pero sustentados siempre en nuestra creatividad</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
<?php include_once('../assets/contenido/footer.php'); ?>