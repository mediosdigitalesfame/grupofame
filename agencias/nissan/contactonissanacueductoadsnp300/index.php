<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Nissan Acueducto</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headernissancontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Nissan Acueducto</h2>
			</div>
		</div>
 
	 <div class="contact-box">
		 <div class="container">
			 <div class="row">

				 <div class="col-md-6" align="center">
                     <div class="container">
					     <div class="col-md-12" >
							<?php include('jotform.php'); ?>
                         </div>
                     </div>
                 </div>

                 <div class="col-md-3">
					 <div class="contact-information">
						 <h3>Información de Contacto</h3>
						 <ul class="contact-information-list">
							 <li><span><i class="fa fa-home"></i> Av. Acueducto 3835, Fray Antonio de Lisboa. Morelia, Michoacán. México, 58254 <strong></strong></span></li>
							 <li><i class="fa fa-phone"></i><span>Ventas <strong>(443) 324-1246</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Servicio <strong>(443) 324-1245 Ext 205 y 309</strong></span><br>
									<i class="fa fa-phone"></i><span>Refacciones <strong>(443) 324-1246 Ext. 301</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Seminuevos <strong>(443) 324-1246 Ext. 504</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Hojalateria y Pintura <strong>(443) 324-1246</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Financiamiento <strong>(443) 324-1246</strong></span><br>
                                    </li>
                         </ul>

                         <p><img src="../../../assets/images/s-agencias/nissan.png"></p>
									<p align="center"> <a href="https://www.nissanacueducto.com.mx/" target="_blank"><strong>www.nissanacueducto.com.mx</strong></a></p>

					 </div>
				 </div>

				 <div class="col-md-3">
					 <div class="contact-information">
						 <h3>Horario de Atención</h3>
						 <p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en Grupo FAME te escuchamos y atendemos de manera personalizada. </p>
						 <p class="work-time"><span>Lunes - Viernes</span> : 9:00 AM a 2:00 PM - 4:00 AM a 7:00 PM</p>
						 <p class="work-time"><span>Sábado</span> : 9:00 AM - 1:00 PM.</p>
                         <p class="work-time"><span>Domingo</span> <i>Cerrado</i></p>
					 </div>
				 </div>

			 </div>
		 </div>
	 </div>
			
 </div>



<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>