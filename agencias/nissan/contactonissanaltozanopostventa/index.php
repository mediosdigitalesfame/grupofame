<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Nissan Altozano</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headernissancontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Nissan Altozano</h2>
			</div>
		</div>
 
	 <div class="contact-box">
		 <div class="container">
			 <div class="row">

				 <div class="col-md-6" align="center">
                     <div class="container">
					     <div class="col-md-12" >
							 <?php include('../../../assets/formularios/formnissanaltpostventa.php'); ?>
                         </div>
                     </div>
                 </div>

                	<div class="col-md-3">
						<div class="contact-information">
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i>Av Montaña Monarca #1000, Desarrollo Montaña Monarca, 58350 Morelia, Mich.<strong></strong></span></li>
								<li><a href="#"><i class="fa fa-phone"></i>01 443 688 38 02 </a></li>
								<li><a href="#"><i class="fa fa-envelope"></i>ventas@nissanaltozano.com.mx</a></li>
							</ul>
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Horario de Atención</h3>
							<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en Grupo FAME te escuchamos y atendemos de manera personalizada. </p>
							<p class="work-time">
								<span>
									Ventas: <br> Lunes - Domingo
								</span> 
								: 11:00 AM a 08:00 PM 
							</p>

							<br>

							<p class="work-time">
								<span>
									Servicio: <br>
								Lunes 
							</span>
								: Cerrado
								<span>
									<br> Martes - Viernes
								</span> 
								: 09:00 AM a 06:00 PM 
								<span>
									<br> Sabado - Domingo
								</span> 
								: 09:00 AM a 04:00 PM 
							</p>

							<br>

							<p class="work-time">
								<span>
									Refacciones: <br>
								Lunes 
							</span>
								: Cerrado
								<span>
									<br> Martes - Viernes
								</span> 
								: 09:00 AM a 06:00 PM 
								<span>
									<br> Sabado - Domingo
								</span> 
								: 09:00 AM a 04:00 PM 
							</p>

							<br>

							<p class="work-time">
								<span>
									Financiamiento: <br> Lunes - Domingo
								</span> 
								: 11:00 AM a 08:00 PM 
							</p>

							<br>

							<p class="work-time">
								<span>
									Caja: <br>
								Lunes 
							</span>
								: Cerrado
								<span>
									<br> Martes - Viernes
								</span> 
								: 09:00 AM a 02:00 PM y 03:00 PM a 06:00 PM
								<span>
									<br> Sabado - Domingo
								</span> 
								: 09:00 AM a 04:00 PM 
							</p>

							<br>

						</div>
					</div>

			 </div>
		 </div>
	 </div>
			
 </div>



<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>