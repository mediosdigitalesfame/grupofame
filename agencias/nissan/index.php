<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Nissan</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headernissan.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Nissan</h2>
			</div>
		</div>

		<!-- blog-box Banner -->
		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">

						<div class="single-project-content">
							<div class="mfp-iframe-holder">

								<div class="col-md-4"> 
									<h2>FAME Nissan Acueducto</h2>
									<p>Te esperamos con gusto en: <br>
										<strong>Av. Acueducto 3835, Colonia Fray Antonio de Lisboa <br>
										Morelia, Michoacán C.P. 58254 Teléfono: (443) 324-1246</strong></p><img src="../../assets/images/s-agencias/nissan.png"></p>
										<p><a href="http://www.nissanacueducto.com.mx" target="_blank"><strong>www.nissanacueducto.com.mx</strong></a></p>
									</div>

									<div class="col-md-8">
										<iframe width='100%' height='500px' frameBorder='0' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d939.1021457338001!2d-101.14850360241033!3d19.695221552811617!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d121d6e46804f%3A0x4135cd2a3e029435!2sFAME+Nissan+Acueducto+Morelia!5e0!3m2!1ses!2smx!4v1416945642181'></iframe>

										<small><a target="_blank" href="https://goo.gl/maps/8LWlQ" style="color:#848484;text-align:left">Ver en Street View</a></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<img src="../../assets/images/shadow.png" style="width:100%;">

			<!-- blog-box Banner -->
			<div class="single-project-page">
				<div class="container">
					<div class="row">

						<div class="col-md-12">

							<div class="single-project-content">
								<div class="mfp-iframe-holder">

									<div class="col-md-4"> 
										<h2>FAME Nissan Altozano</h2>
										<p>Te esperamos con gusto en: <br>
											<strong>Av Montaña Monarca #1000, Desarrollo Montaña Monarca, 58350 Morelia, Mich. <br>
											Morelia, Michoacán C.P. 58254 Teléfono: (443) 324-1246</strong></p><img src="../../assets/images/s-agencias/nissan.png"></p>
											<p><a href="http://www.nissanaltozano.com.mx" target="_blank"><strong>www.nissanaltozano.com.mx</strong></a></p>
										</div>

										<div class="col-md-8">
											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15027.018610966603!2d-101.16553856402412!3d19.680469992211584!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0d3f09a56753%3A0xc2a35ed107da2c54!2sNissan+Altozano!5e0!3m2!1ses-419!2smx!4v1534003391044" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

											<small><a target="_blank" href="https://www.google.com.mx/maps/@19.6739479,-101.1613056,3a,75y,5.92h,98t/data=!3m6!1e1!3m4!1slKn-04WCie9VbMInTLspNQ!2e0!7i13312!8i6656" style="color:#848484;text-align:left">Ver en Street View</a></small>
										</div>

									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<?php include('../../assets/contenido/agencias/footer.php'); ?>