<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Mitsubishi</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headermitsubishi.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Mitsubishi</h2>
			</div>
		</div>

		<!-- blog-box Banner -->
		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						<div class="col-md-4"> <br><br><br>                    
							<h2>FAME Mitsubishi Querétaro</h2>
							<p>Te esperamos con gusto en: <br>
								<strong>Prolongación Av. Luis M. Vega #302 Ampliación Cimatario<br>
								Querétaro, Qro. C.P. 76030 Teléfono: (442) 214 5150</strong></p><br><br><p><img src="../../assets/images/s-agencias/mitsubishi.png"></p>
								<p><a href="http://www.mitsubishiqueretaro.com.mx/" target="_blank"><strong>www.mitsubishicorregidora.com</strong></a></p>
							</div>
							<div class="col-md-8">
								<div class="single-project-content">
									<div class="mfp-iframe-holder">
										<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3735.2747370894094!2d-100.389808!3d20.576834999999996!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d344d9e5a3cb6d%3A0x8b6f929eb7c9cb51!2sMitsubishi+Queretaro!5e0!3m2!1ses-419!2smx!4v1417711758159"></iframe>

										<small><a target="_blank" href="https://goo.gl/maps/BMDAk">Ver en Street View</a></small>
									</div></div></div></div>
									<img src="../../assets/images/shadow.png" style="width:100%;">         
									<div class="single-project-content">
										<div class="col-md-4">
											<h2>FAME Mitsubishi Uruapan</h2>
											<p>Te esperamos con gusto en: <br>
												<strong>Prolongación Paseo Lázaro Cárdenas #500 Fraccionamiento Jardines del Bosque<br>
												Uruapan, Michoacán. C.P. 60190 Teléfono: (452) 527 5700</strong></p><p><img src="../../assets/images/s-agencias/mitsubishi.png"></p>
												<p><a href="http://www.mitsubishiuruapan.com.mx/" target="_blank"><strong>www.mitsubishiuruapan.com.mx</strong></a></p>
											</div>  
											<div class="col-md-8">

												<div class="mfp-iframe-holder">
													<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3763.7271164197846!2d-102.05919899999999!3d19.380965!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de29796a3609f%3A0xcd856d79bad2017d!2sFAME+Mitsubishi+Uruapan!5e0!3m2!1ses-419!2smx!4v1417712348935"></iframe></div>

													<a href="https://goo.gl/maps/xNxqm">Ver en Street View</a></small>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<!-- End content -->

							<?php include('../../assets/contenido/agencias/footer.php'); ?>