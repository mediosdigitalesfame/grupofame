<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Honda Manantiales</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerhondacontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Honda Manantiales</h2>
			</div>
		</div>

			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								       <?php include('../../../assets/formularios/formhonamanan.php'); ?>
                                     </div>
                                 </div>
                             </div>
                        
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Periférico Paseo de la República #601</span> <span>Col. Los Manantiales. </span> <span> Morelia, Michoacán</span></li>
									<li><span><i class="fa fa-phone"></i>(443) 340 0160</span></li>
                                    <li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 100</strong></span><br>
									<i class="fa fa-phone"></i><span>Gerencia de Servicio<strong>Ext. 102</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Refacciones <strong>Ext.403</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong>Ext. 201</strong></span><br>                                    
                                    <i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 300</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Gerencia Seminuevos<strong>Ext. 301</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Contabilidad <strong>Ext. 401</strong></span><br>  
                                     <i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 205</strong></span><br>                            
                                    </li>
                                    <h3>Whatsapp</h3>
    								<li><span><i class="fa fa-whatsapp"></i><strong>   Ventas   y   Postventa <br> 4432732790 | 4432718558</strong></span></li>

									<li><a href="#"><i class="fa fa-envelope"></i>contacto@hondamanantiales.com</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>HONDA FAME Manantiales</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Sábado</span> : 8:00 a.m. - 8:00 p.m.</p>
							</div>
						</div>
 
					</div>
				</div>
			</div>

		</div> 

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>