<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Honda</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headerhonda.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Honda</h2>
			</div>
		</div>

		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">

						<div class="single-project-content">
							<div class="mfp-iframe-holder">
								<div class="col-md-4">
									<h2>FAME Honda Atizapán </h2><br>                               
									<p> <strong> Te esperamos con gusto en: </strong><br>
										Circuito Adolfo Ruíz Cortínez #353. Lote 10 Manzana 1. Colonia Las Margaritas. Atizapán de Zaragoza, Estado de México. CP 52977<br><br>
										<span><i class="fa fa-phone"></i></span> <strong>Teléfono: (551) 668 9252</strong><br>
										<span><i class="fa fa-envelope"></i></span>
									</p>
									<a href="http://hondaatizapan.com/" target="_blank"> <strong>www.hondaatizapan.com</strong></a>
									<p>
										<div class="col-md-12"><img src="../../assets/images/s-agencias/honda.png"></div>
									</div>

									<div class="col-md-8">                                 
										<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3759.818229597169!2d-99.27517215501709!3d19.549415895061692!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTnCsDMyJzU5LjUiTiA5OcKwMTYnMzUuMSJX!5e0!3m2!1ses-419!2smx!4v1419364508732"></iframe>

										<small><a target="_blank" href="https://www.google.com.mx/maps/@19.54973,-99.2761112,3a,75y,255.97h,94.35t/data=!3m4!1e1!3m2!1sW9auy6QUM4DbakcgHeYJNQ!2e0!6m1!1e1" style="color:#848484;text-align:left">Ver en Street View</a></small>
									</div>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>

			<img src="../../assets/images/shadow.png" style="width:100%;">

			<div class="single-project-page">
				<div class="container">
					<div class="row">

						<div class="col-md-12">

							<div class="single-project-content">
								<div class="mfp-iframe-holder">
									<div class="col-md-4" id="altozano">
										<h2>FAME Honda Altozano</h2> <br>

										<p>
											<strong> Te esperamos con gusto en: </strong><br>
											Zona Automotriz Centro Comercial Paseo Altozano. Morelia, Michoacán.<br><br>
											<span><i class="fa fa-phone"></i></span> <strong>Teléfono: (443) 340 45 37</strong><br>
											<span><i class="fa fa-envelope"></i></span>
										</p>
										<p>
											<a href="http://hondaaltozano.com/" target="_blank"><strong> www.hondaaltozano.com</strong></a>
										</p>
										<div class="col-md-12"><img src="../../assets/images/s-agencias/honda.png"></div>
									</div>

									<div class="col-md-8">                                 
										<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7513.851380091018!2d-101.16152891693628!3d19.67317543926401!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d120a88d7dc89%3A0x945ba1aec77d55cf!2sFAME+Honda+Altozano!5e0!3m2!1ses!2smx!4v1417112367677"></iframe>

										<small><a target="_blank" href="https://goo.gl/maps/KAXHyBtTVkQ21" style="color:#848484;text-align:left">Ver en Street View</a></small>
									</div>
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>

			<img src="../../assets/images/shadow.png" style="width:100%;">

			<div class="single-project-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" id="manantiales">
								<div class="single-project-content">

									<br><br><br><h2>FAME Honda Manantiales <br> </h2> 
									<h3></h3><br>

									<p><strong> Te esperamos con gusto en: </strong><br>
										Periferico Paseo de la República No.511. Los Manantiales, Zona Sin Asignación de Nombre de Colonia, 58290 Morelia, Mich.
										<br><br>
										<span><i class="fa fa-phone"></i></span> <strong>Teléfono: (443) 340 01 60</strong><br>
										<span><i class="fa fa-envelope"></i></span><strong><a href="http://hondamanantiales.com/" target="_blank"> www.hondamanantiales.com</a></strong>
									</p>

									<p><div class="col-md-10"><img src="../../assets/images/s-agencias/honda.png"></div></p>

								</div>
							</div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">

									<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15026.048695653208!2d-101.238896!3d19.690807!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1899fe2ab368acb4!2sHonda+Manantiales!5e0!3m2!1ses-419!2smx!4v1481070332788" width="100%" height="500" frameborder="0"></iframe>
									<a name="manantiales"></a>
									<small><a target="_blank" href="https://goo.gl/maps/HySYet1XgTQ2" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="../../assets/images/shadow.png" style="width:100%;">
			<!-- ---------------Ubicacion 3---------------------- -->

			<!-- ------------------Ubicacion 4 ----------------------- -->

			<div class="single-project-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" id="corregidora">
								<div class="single-project-content">

									<br><br><br><h2>FAME Honda Corregidora <br> </h2> 
									<h3></h3><br>

									<p><strong> Te esperamos con gusto en: </strong><br>
										Lateral Autopista México-Querétaro # 2108 Col. Parque Industrial Papanoa. Querétaro, Qro. C.P. 76080 
										<br><br>
										<span><i class="fa fa-phone"></i></span> <strong>Teléfono: (442) 309 4000</strong><br>
										<span><i class="fa fa-envelope"></i></span><strong><a href="http://www.famecorregidora.com/" target="_blank"> www.famecorregidora.com</a></strong>
									</p>

									<p><div class="col-md-10"><img src="../../assets/images/s-agencias/honda.png"></div></p>

								</div>
							</div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">

									<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3735.293486629277!2d-100.3918111140879!3d20.57606889544557!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d344d9b39e75e3%3A0x86a2827b4a6e9d46!2sFAME+Honda+Corregidora!5e0!3m2!1ses!2smx!4v1417649617637"></iframe>
									<a name="corregidora"></a>
									<small><a target="_blank" href="https://goo.gl/maps/LL6Tb" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="../../assets/images/shadow.png" style="width:100%;">
			<!-- ---------------Ubicacion 4---------------------- -->


			<!-- ------------------Ubicacion 5 ----------------------- -->

			<div class="single-project-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" id="marquesa">
								<div class="single-project-content">

									<br><br><br><h2>FAME Honda Marquesa <br> </h2> 
									<h3></h3><br>

									<p><strong> Te esperamos con gusto en: </strong><br>
										Blvd. Bernardo Quintana #622 Pte. Col. Desarrollo San Pablo. Queretaro, Qro. C.P. 76130
										<br><br>
										<span><i class="fa fa-phone"></i></span> <strong>Teléfono: (442) 209 7900</strong><br>
										<span><i class="fa fa-envelope"></i></span><strong><a href="http://www.famemarquesa.com/" target="_blank">www.famemarquesa.com</a></strong>
									</p>

									<p><div class="col-md-10"><img src="../../assets/images/s-agencias/honda.png"></div></p>

								</div>
							</div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">


									<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1867.1292714732763!2d-100.40855573033937!3d20.618315870247567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0x6b8ba25b5739322f!2sHonda!5e0!3m2!1ses!2smx!4v1417651513504"></iframe>
									<a name="marquesa"></a>
									<small><a target="_blank" href="https://goo.gl/maps/rKVoB" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="../../assets/images/shadow.png" style="width:100%;">
			<!-----------------Ubicacion 5------------------------>

			<!--------------------Ubicacion 6 ----------------------- -->

			<div class="single-project-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" id="monarca">
								<div class="single-project-content">

									<br><br><br><h2>FAME Honda Monarca Morelia <br> </h2> 
									<h3></h3><br>

									<p><strong> Te esperamos con gusto en: </strong><br>
										Av. Acueducto No. 2865 Col. Lomas de Hidalgo. Morelia Mich. 
										<br><br>
										<span><i class="fa fa-phone"></i></span> <strong>Teléfono:(443) 315 22 44</strong><br>
										<span><i class="fa fa-envelope"></i></span><strong><a href="http://famemonarca.com/" target="_blank">www.famemonarca.com</a></strong>
									</p>

									<p><div class="col-md-10"><img src="../../assets/images/s-agencias/honda.png"></div></p>

								</div>
							</div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">

									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.3916517968687!2d-101.16018368508918!3d19.695942986735236!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf1532b368d2b0a3a!2sFAME+Honda+Monarca!5e0!3m2!1ses!2s!4v1491590075326" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
									<a name="monarca"></a>
									<small><a target="_blank" href="https://goo.gl/maps/9xK4X" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="../../assets/images/shadow.png" style="width:100%;">
			<!-----------------Ubicacion 6------------------------>


			<!--------------------Ubicacion 7 ----------------------- -->

			<div class="single-project-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" id="monarcadf">
								<div class="single-project-content">

									<br><br><br><h2>FAME Honda Monarca D.F. <br> </h2> 
									<h3></h3><br>

									<p><strong> Te esperamos con gusto en: </strong><br>
										Av. Doctor Rio de la Loza #45. Col. Doctores. Ciudad de México.  
										<br><br>
										<span><i class="fa fa-phone"></i></span> <strong>Tel: (55) 55 88 56 88</strong><br>
										<span><i class="fa fa-envelope"></i></span><strong><a href="http://famemonarcadf.com/" target="_blank">www.famemonarcadf.com</a></strong>
									</p>

									<p><div class="col-md-10"><img src="../../assets/images/s-agencias/honda.png"></div></p>

								</div>
							</div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">


									<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3762.7038885828515!2d-99.14453800000001!3d19.425196000000003!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d1fed63eaf8461%3A0xf27530b408735dda!2sFAME+Honda+Monarca+D.F.!5e0!3m2!1ses-419!2smx!4v1417653831821"></iframe>
									<a name="monarcadf"></a>
									<small><a target="_blank" href="https://goo.gl/maps/bD1kT" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="../../assets/images/shadow.png" style="width:100%;">
			<!-----------------Ubicacion 7------------------------>

			<!--------------------Ubicacion 8 ----------------------- -->

			<div class="single-project-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" id="sanjuan">
								<div class="single-project-content">

									<br><br><br><h2>FAME Honda San Juan del Río<br> </h2> 
									<h3></h3><br>

									<p><strong> Te esperamos con gusto en: </strong><br>
										Av. Paseo Central #149 Col. San Cayetano. San Juan del Río, Querétaro.
										<br><br>
										<span><i class="fa fa-phone"></i></span> <strong> Tel: (427) 274 9586</strong><br>
										<span><i class="fa fa-envelope"></i></span><strong><a href="http://hondasanjuandelrio.com" target="_blank">www.hondasanjuandelrio.com</a></strong>
									</p>

									<p><div class="col-md-10"><img src="../../assets/images/s-agencias/honda.png"></div></p>

								</div>
							</div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">


									<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3739.913792808695!2d-99.98505499999999!3d20.386443999999997!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d30c86988ec97d%3A0x441f8749da965c58!2sHonda+San+Juan+del+Rio!5e0!3m2!1ses-419!2smx!4v1417654062856"></iframe>
									<a name="sanjuan"></a>
									<small><a target="_blank" href="https://goo.gl/maps/GOkcK" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="../../assets/images/shadow.png" style="width:100%;">
			<!-----------------Ubicacion 8------------------------>

			<!--------------------Ubicacion 9 ----------------------- -->

			<div class="single-project-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" id="uruapan">
								<div class="single-project-content">

									<br><br><br><h2>FAME Honda Uruapan<br> </h2> 
									<h3></h3><br>

									<p><strong> Te esperamos con gusto en: </strong><br>
										Paseo Lázaro Cárdenas #451  Col. Morelos.Uruapan, Michoacán. 
										<br><br>
										<span><i class="fa fa-phone"></i></span> <strong> Tel: (452) 524 75 00</strong><br>
										<span><i class="fa fa-envelope"></i></span><strong><a href="http://hdu.com.mx" target="_blank"> www.hdu.com.mx</a></strong>
									</p>

									<p><div class="col-md-10"><img src="../../assets/images/s-agencias/honda.png"></div></p>

								</div>
							</div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">


									<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1881.4927655338631!2d-102.05181326256105!3d19.413031124085997!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de2664c9099c5%3A0xa3a0831f37e2d642!2sFAME+Honda+Uruapan!5e0!3m2!1ses-419!2smx!4v1417654388903"></iframe>
									<a name="uruapan"></a>
									<small><a target="_blank" href="https://goo.gl/maps/sNaCA" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<img src="../../assets/images/shadow.png" style="width:100%;">

			<!-----------------Ubicacion 9------------------------>

			<!--------------------Ubicacion 10 ----------------------- -->

			<div class="single-project-page">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="col-md-4" id="feb">
								<div class="single-project-content">

									<br><br><br><h2>FAME Honda 5 de febrero<br> </h2> 
									<h3></h3><br>

									<p><strong> Te esperamos con gusto en: </strong><br>
										Av. 5 de Febrero #1502 Colonia San Pablo. Querétaro, QRO.
										<br><br>
										<span><i class="fa fa-phone"></i></span> <strong> Tel: (442) 210 2404</strong><br>
										<span><i class="fa fa-envelope"></i></span><strong><a href="http://honda5defebrero.com/" target="_blank">www.honda5defebrero.com</a></strong>
									</p>

									<p><div class="col-md-10"><img src="../../assets/images/s-agencias/honda.png"></div></p>

								</div>
							</div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">

									<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d660.1616125751121!2d-100.41915364183144!3d20.610995313533028!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8d2e6a7081179889!2sFAME+Honda+5+de+Febrero!5e0!3m2!1ses-419!2smx!4v1471016533914" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>

									<a name="5defeb"></a>
									<small><a target="_blank" href="https://goo.gl/maps/cioxBw1bLzk" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<?php include('../../assets/contenido/agencias/footer.php'); ?>