<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Honda San Juan del Río </title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerhondacontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - San Juan del Río</h2>
			</div>
		</div>

			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('../../../assets/formularios/formhonsanju.php'); ?>
                                     </div>
                                 </div>
                             </div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Av. Paseo Central #149</span> <span>Col. San Cayetano</span> <span>San Juan del Río, Querétaro.</span></li>
									<li><span><i class="fa fa-phone"></i>(427) 274 9586 / 87</span></li>
                                    <h3>Whatsapp</h3>
                                    <li>
                                        <span><i class="fa fa-whatsapp"></i><strong>   Ventas   y   Postventa <br> 4431810975 | 4433952597</strong></span>
                                    </li>
                                     <li>
                                        <span><i class="fa fa-whatsapp"></i><strong>   Servicios <br> 4271521319 | 4271520188 </strong></span>
                                    </li>
                                    <li>
                                        <i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 120</strong></span><br>
                                    
				    <i class="fa fa-phone"></i><span>Postventa <strong>Ext. 121</strong></span><br>                                    
				    <i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 113</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong>Ext. 101</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Recepción <strong>Ext. 100</strong></span><br>                                    
                                    <i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 106</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Contabilidad <strong>Ext. 111</strong></span><br>                                   
                                    </li>

								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda San Juan del Río, Querétaro</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>
							</div>
						</div>
 
					</div>
				</div>
			</div>

		</div> 

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>