<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Honda Atizapan</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerhondacontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Honda Atizapan</h2>
			</div>
		</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">
                         <div class="container">
					         <div class="col-md-12" >
								 <?php include('../../../assets/formularios/formhonatiz.php'); ?>
                             </div>
                         </div>
                     </div>
    
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Circuito Ruiz Cortines #353.</span> <span>Lote 10, Manzana 1. </span><span>Colonia Las Margaritas. CP 52977.</span><span> Atizapán de Zaragoza, Estado de México.</span></li>
									<li><span><i class="fa fa-phone"></i>(55) 1668 9252</span></li>
									<h3>Whatsapp</h3>
                                         
                                         <li><span><i class="fa fa-whatsapp"></i><strong>   Ventas   y   Postventa <br> 5554187412 |  5529660346</strong></span></li>
                                         <li><span><i class="fa fa-whatsapp"></i><strong>   Citas   y   Servicio <br> 5513338485 |  5554186816</strong></span></li>
                                    
                                    <li><a href="#"><i class="fa fa-envelope"></i>recepcion@hondaatizapan.com</a></li>
                                    
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en FAME Honda Atizapán; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 8:00  - 19:00 hrs.</p>
								<p class="work-time"><span>Sábado</span> : 8:00  - 14:00 hrs.</p>
							</div>
						</div>

 </div>
			 </div>
		 </div>

		</div> 


<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>