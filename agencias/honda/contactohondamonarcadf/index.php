<!doctype html>
<html lang="es" class="no-js">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Honda Monarca DF</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerhondacontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Honda Monarca DF</h2>
			</div>
		</div>

			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

						 <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('../../../assets/formularios/formfamemondf.php'); ?>
                                     </div>
                                 </div>
                             </div>

                             
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Av. Doctor Río de la Loza #45</span> <span>Col. Doctores</span> <span>Delegación Cuauhtémoc, México, Distrito Federal.</span></li>
									<li><span><i class="fa fa-phone"></i>(55) 5588 5688</span></li>
                                    <li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 117</strong></span><br>
                                    
						<i class="fa fa-phone"></i><span>Postventa <strong>Ext. 124</strong></span><br>                                    
									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 113</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong>Ext. 106</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Recepción <strong>Ext. 110</strong></span><br>                                    
                                    <i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 120</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Contabilidad <strong>Ext. 217</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 105</strong></span><br>  
                                    </li>
										<h3>Whatsapp</h3>
											<li><span><i class="fa fa-whatsapp"></i><strong> Ventas y Postventa <br> 5543574803  |  5513537530 </strong></span></li>
											<li><span><i class="fa fa-whatsapp"></i><strong>   Citas   y   Servicio <br> 5537311738 |  5554187976</strong></span></li>
											<li><span><i class="fa fa-whatsapp"></i><strong>   Calidad <br> 5538997738  </strong></span></li>

								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda Monarca, D.F.</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 8:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 5:00 p.m.</p>
								<p class="work-time"><span>Domingo</span> : 11:00 a.m. - 5:00 p.m.</p>
							</div>
						</div>

						 
 
					</div>
				</div>
			</div>

		</div> 

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>