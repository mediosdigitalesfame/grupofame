<!doctype html>
<html lang="es" class="no-js">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Honda Marquesa</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerhondacontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Honda Marquesa</h2>
			</div>
		</div>

			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">

                        	 <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('../../../assets/formularios/formfamemarq.php'); ?>
                                     </div>
                             </div>
						</div>
						<div class="col-md-6">
                            <div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Honda Marquesa</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m. | <span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>
									</div>
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i>Blvd. Bernardo Quintana #622 Col. Desarrollo San Pablo C.P 76130 Querétaro, Qro</span></li>
								<li><span><i class="fa fa-phone"></i>Agencia : <strong>(442) 209 7900</strong></span></li>
                                <li>
                                <i class="fa fa-phone"></i><span>NÚMERO CONMUTADOR:<strong> (442) 209-7900</strong></span><br>
                                <i class="fa fa-phone"></i><span>POSTVENTA:	<strong> (442) 209-7910</strong></span><br>
                                <i class="fa fa-phone"></i><span>REFACCIONES: <strong> (442) 209-7909</strong></span><br>
                                <i class="fa fa-phone"></i><span>CREDITO Y SEGUROS: <strong> (442) 209-7915</strong></span><br> <i class="fa fa-phone"></i><span>GERENCIA GENERAL: <strong> (442) 209-7911</strong></span><br> 
                                <i class="fa fa-phone"></i><span>RECEPCION: <strong>(442) 209-7900  *presionar 0</strong></span><br> 
                                <i class="fa fa-phone"></i><span>SEMINUEVOS: <strong>(442) 209-7900 *presionar 4</strong></span><br> 
                                <i class="fa fa-phone"></i><span>TALLER DE SERVICIO CITAS: <strong> (442) 209-7912 | (442) 209-7913 | (442) 209-7914</strong></span><br>   
                                <i class="fa fa-phone"></i><span>VENTAS: <strong> (442) 209-7901 | (442) 209-7902 | (442) 209-7911</strong></span><br>       
                                </li>
                                <h3>Whatsapp</h3>
                                <li><span>
                                <i class="fa fa-whatsapp"></i> CITAS SERVICIO: <strong>443-202-6580</strong><br>
                                <i class="fa fa-whatsapp"></i> REFACCIONES: <strong>443-273-2778</strong><br>
                                <i class="fa fa-whatsapp"></i> ATENCIÓN A CLIENTES: <strong>443-229-4955</strong><br>
                                <i class="fa fa-whatsapp"></i> VENTAS NUEVOS:<strong>443-202-8303</strong><br>
                                <i class="fa fa-whatsapp"></i> VENTAS USADAS:<strong>443-218-2765</strong><br>
                                    </span></li>

									<li><a href="#"><i class="fa fa-envelope"></i>recepcion@famemarquesa.com</a></li>
								</ul>
							</div>
						</div>
					

					</div>
				</div>
			</div>

		</div>

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>