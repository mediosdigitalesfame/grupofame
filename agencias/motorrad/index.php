<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - BMW Motorrad</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headermotorrad.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>BMW Motorrad</h2>
			</div>
		</div>

		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">

						<div class="single-project-content">
							<div class="mfp-iframe-holder">

								<div class="col-md-4">
									<br><br><br><h2>BMW Motorrad</h2>
									<p>
										Te esperamos con gusto en: <br>
										<strong>Av. Acueducto 2936, Colonia Poblado Ocolusen <br>
											Morelia, Michoacán C.P. 58254 <br>Teléfono:(443) 324-77-29 y 314-41-76</strong></p>
										</p>
										<p>
											<a href="http://www.motorradmorelia.com/" target="_blank"><strong>www.motorradmorelia.com/</strong></a>
										</p>
										<div class="col-md-6">
											<img src="../../assets/images/s-agencias/motorrad.png">
										</div>
									</div>

									<div class="col-md-8">
										<iframe width='100%' height='500px' frameBorder='0' src='https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d789.6886345402443!2d-101.15781258680778!3d19.695017515315698!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d1201e0db89fd%3A0xc71975a8ace1524a!2sBMW+Motorrad!5e0!3m2!1ses!2smx!4v1499265908032'></iframe>
										<small><a target="_blank" href="https://goo.gl/maps/8LWlQ" style="color:#848484;text-align:left">Ver en Street View</a></small>
									</div>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</body>
 

		<?php include('../../assets/contenido/agencias/footer.php'); ?>