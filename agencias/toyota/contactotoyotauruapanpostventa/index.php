<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Toyota Uruapan</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headermotorradcontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Toyota Uruapan</h2>
			</div>
		</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
						
                        <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('../../../assets/formularios/formtoyotauruapanpostventa.php'); ?>
                                     </div>
                                 </div>
                             </div>
                        
                        <div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li>
										<span><i class="fa fa-home"></i>Libramiento Oriente No. 5675, </span> 
										<span> Col. Quirindavara,  </span> 
										<span> Uruapan, Michoacán</span>
										<span> C.P. 60190</span> 
									</li>
									<li><span><i class="fa fa-phone"></i>01 (452) 528 8815</span></li>
									    
                                    
                                    </li>
                                    
                               <p>
                                <h3>WhatsApp</h3>

                               <li><span><i class="fa fa-whatsapp"></i><strong> Ventas <br> 

                               	<a href="https://api.whatsapp.com/send?phone=524425923862&text=Hola,%20Quiero%20más%20información!" title="Ventas Nuevos">
												442 592 38 62
											</a>


                                </strong></span></li>


							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Toyota FAME Uruapan</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p><br>
                                
                                <center>
                                <h3>TOYOTA Uruapan</h3>
                                <P class="work-time">Libramiento Oriente No. 5675, </P>
                                <P class="work-time">Col. Quirindavara, Uruapan, Mich. </P>
                                <p class="work-time">C.P. 60190</p>
                                </center>
							</div>
						</div>


				</div>
		</div><br>
        </div>

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>