<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Toyota Perisur</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headermotorradcontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Toyota Perisur</h2>
			</div>
		</div>

		<div class="contact-box">
			<div class="container">
				<div class="row">
					<div class="col-md-6" align="center">
						
						

						
						<?php include('../../../assets/formularios/formfameperipostventa.php'); ?>

					</div>
					
					<div class="col-md-3">
						<div class="contact-information">
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i><span>Anillo Perif. 4000 Bis,</span> <span>Col. Jardines del Pedregal </span> <span> Del. Álvaro Obregón, México D.F.</span><span> C.P. 01900</span> </li>
								<li><span><i class="fa fa-phone"></i>(55) 5481 1900</span><br>
									<span><i class="fa fa-phone"></i>01 800 670 8386</span>
								</li>
								<li><i class="fa fa-phone"></i><span>Recepción <strong>Ext. 100 </strong></span><br>                                    
									<i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 224</strong></span><br>
									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 145 </strong></span><br>
									<i class="fa fa-phone"></i><span>Postventa <strong>Ext. 136 </strong></span><br>
									<i class="fa fa-phone"></i><span>Ventas <strong>Ext. 110 </strong></span><br>      
									<i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 124 </strong></span><br>  </li>                                   
									
									
									<p>
										<h3>Whatsapp</h3>
										<li><span><i class="fa fa-whatsapp"></i><strong>  Ventas   y  Postventa <br>5513843926 | 5532325380 </strong></span></li>
										<li><span><i class="fa fa-whatsapp"></i><strong>  Citas   y  Financiamiento <br>5529221489 | 5539937307 </strong></span></li>
										
									</p>
									<li><a href="#"><i class="fa fa-envelope"></i>recepcion@fameperisur.com</a></li>
									
									
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Toyota FAME Perisur</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<h4>Sala de ventas</h4>
								<p class="work-time"><span>Lunes - Viernes</span>: 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span>: 9:00 a.m. - 6:00 p.m.</p>
								<p class="work-time"><span>Domingo</span>: 11:00 a.m. - 6:00 p.m.</p>                             
							</div>
						</div>

						
					</div>
				</div><br>
			</div>

			<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>