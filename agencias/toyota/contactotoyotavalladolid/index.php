<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Toyota Valladolid</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headermotorradcontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Toyota Valladolid</h2>
			</div>
		</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
						
                        <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								        <?php include('../../../assets/formularios/formfamevall.php'); ?>
                                     </div>
                                 </div>
                             </div>
                        
                        <div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Av. Acueducto #3603</span> <span>Col. Pascual Ortiz de Ayala </span> <span> Morelia, Michoacán</span><span> C.P. 58205</span> </li>
									<li><span><i class="fa fa-phone"></i>(443) 340 0100</span></li>
									<li><i class="fa fa-phone"></i><span>Recepción <strong> </strong></span><br>                                    <i class="fa fa-phone"></i><span>Taller de Servicio <strong> </strong></span><br>
                                   	<i class="fa fa-phone"></i><span>Refacciones <strong> </strong></span><br>
                                    <i class="fa fa-phone"></i><span>Postventa <strong> </strong></span><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong> </strong></span><br>      
                                    
                                    </li>
                                    
                               <p>
                                <h3>Whatsapp</h3>

                               <li><span><i class="fa fa-whatsapp"></i><strong>  Ventas   y  Postventa <br>4433250949 | 4432732830 | 4432732824 </strong></span></li>

                               <li><span><i class="fa fa-whatsapp"></i><strong>  Servicio <br>  4432732820 </strong></span></li>

                               
                              </p>
                                    
									<li><a href="#"><i class="fa fa-envelope"></i>recepcion@famevalladolid.com</a></li>
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Toyota FAME Valladolid</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p><br>
                                
                                <center>
                                <h3>TOYOTA ALTOZANO</h3>
                                <P class="work-time">Av. Montaña Monarca #100 Local R-N-05</P>
                                <P class="work-time">Col. Desarrollo Montaña Monarca C.P.58350</P>
                                <p class="work-time">Morelia, Michoacán.</p>
                                </center>
							</div>
						</div>


                </div>
            </div>
        </div>

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>