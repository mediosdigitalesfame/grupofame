<!doctype html>
<html lang="es" class="no-js">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());
	  gtag('config', 'UA-91159946-1');
	</script>
		<title>Grupo FAME - Toyota</title>
		<?php include('contenido/head.php'); ?>
</head>    

<body>

	<div id="container">
		<?php include('../../assets/contenido/agencias/headertoyota.php'); ?>
		<div class="page-banner">
			<div class="container">
				<h2>Toyota</h2>
			</div>
		</div>

		<!-- blog-box Banner -->
		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						
						<div class="single-project-content">
							<div class="col-md-4"><br><br><br>
								<h2>FAME Toyota Perisur</h2>
								<p>Te esperamos con gusto en: <br>
								<strong>Adolfo Ruiz Cortines 4000-BIS Col. Jardines del Pedregal<br>
                                 Del. Álvaro Obregón, México D.F. C.P. 01900<br> Teléfono: (55) 5481 1900</strong></p>
                                 <p><img src="../../assets/images/s-agencias/toyota.png"></p>
                                 <p><a href="http://www.fameperisur.com/" target="_blank"><strong>www.fameperisur.com</strong></a></p>
                            </div>
							<div class="col-md-8">
								<div class="mfp-iframe-holder">
									<a name="perisur"></a>
									<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3765.4142188173223!2d-99.214421!3d19.307824!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85cdffbf04b1453f%3A0x4be68c599a185ef7!2sFAME+Toyota+Perisur!5e0!3m2!1ses-419!2smx!4v1417655165014"></iframe>
									<small><a target="_blank" href="https://goo.gl/maps/0S71P">Ver en Street View</a></small>
								</div>
							</div>
						</div>

						<img src="../../assets/images/shadow.png" style="width:100%;">
										
						<div class="single-project-content">
							<div class="col-md-4"><br><br><br>
								<h2>FAME Toyota Valladolid</h2>
								<p>Te esperamos con gusto en: <br>
								<strong>Avenida Acueducto # 3603 Col. Pascual Ortiz de Ayala<br>
								Morelia, Michoacán. C.P. 58250<br> Teléfono: (443) 340 0100</strong></p></p><img src="../../assets/images/s-agencias/toyota.png"></p>
								<p><a href="http://www.famevalladolid.com/" target="_blank"><strong>www.famevalladolid.com</strong></a></p>
							</div>  
							<div class="col-md-8">
								<div class="mfp-iframe-holder">
									<a name="valladolid"></a><small>
									<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1878.2067738490991!2d-101.15055600000001!3d19.695009999999996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d1202b2fd839b%3A0x2fba13045742d483!2sFAME+Toyota+Morelia!5e0!3m2!1ses-419!2smx!4v1417042727777"></iframe>
									<a href="https://goo.gl/maps/AISou">Ver en Street View</a></small>
								</div> 
							</div>
						</div>
												
						<img src="../../assets/images/shadow.png" style="width:100%;">

						<div class="single-project-content">
							<div class="col-md-4"><br><br><br>
								<h2>Toyota Altozano FAME</h2>
								<p>Te esperamos con gusto en: <br>
								<strong>Zona Automotriz Centro Comercial Paseo Altozano. Morelia, Michoacán.<br> Teléfono: (443) 147 10 44/36 </strong></p></p><img src="../../assets/images/s-agencias/toyota.png"></p>
							</div>  
							<div class="col-md-8">
								<div class="mfp-iframe-holder">
									<a name="valladolid"></a><small>
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d939.2237301535325!2d-101.16178086633059!3d19.674487913282274!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x8aaf12ed4a4d1444!2sToyota+Altozano+FAME!5e0!3m2!1ses-419!2smx!4v1544807713667" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
									<a href="https://goo.gl/maps/AISou">Ver en Street View</a></small>
								</div> 
							</div>
						</div>
											
						<img src="../../assets/images/shadow.png" style="width:100%;">

						<div class="single-project-content">
							<div class="col-md-4"><br><br><br>
								<h2>FAME Toyota Uruapan</h2>
								<p>Te esperamos con gusto en: <br>
								<strong>Libramiento Oriente No. 5675, Col. Quirindavara, <br>
								Uruapan, Michoacán C.P. 60190<br> Teléfono: 01 (452) 528 8815</strong></p></p><img src="../../assets/images/s-agencias/toyota.png"></p>
								<p><a href="http://www.toyotauruapan.com/" target="_blank"><strong>www.toyotauruapan.com</strong></a></p>
							</div>  
							<div class="col-md-8">
								<div class="mfp-iframe-holder">
									<a name="uruapan"></a><small>
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.716727081825!2d-102.06089588509407!3d19.381414586913436!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xa210a2f9a7f3bf16!2sFAME+Toyota+Uruapan!5e0!3m2!1ses-419!2smx!4v1542931711924" width='100%' height="500" frameborder="0" style="border:0" allowfullscreen></iframe></small>
								</div>
							</div>
						</div>

						<div class="single-project-content">
							<div class="col-md-4"><br><br><br>
								<h2>FAME Toyota La Piedad</h2>
								<p>Te esperamos con gusto en: <br>
								<strong>Blvd. Lázaro Cárdenas No. 3036, Fracc. Camelinas, <br>
								La Piedad, Michoacán C.P. 59374<br> Teléfono: 01 352 526 8821</strong></p></p><img src="../../assets/images/s-agencias/toyota.png"></p>
								<p><a href="http://www.toyotalapiedad.com/" target="_blank"><strong>www.toyotalapiedad.com</strong></a></p>
							</div>  
							<div class="col-md-8">
								<div class="mfp-iframe-holder">
									<a name="uruapan"></a><small>
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7481.428400340448!2d-102.05532202358785!3d20.353422154473442!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xca85958d39872cb5!2sToyota+La+Piedad+FAME!5e0!3m2!1ses-419!2smx!4v1554570935056!5m2!1ses-419!2smx" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe></small>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>

</body>
		<!-- End content -->
        <?php include('../../assets/contenido/agencias/footer.php'); ?>

