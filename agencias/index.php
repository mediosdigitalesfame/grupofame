<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Agencias</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		<?php include('../assets/contenido/agencias/headeragencias.php'); ?> 


		<div id="content">

			<!-- Page Banner -->
			<div class="page-banner">
				<div class="container">
					<h2>Agencias</h2>
					
				</div>  
			</div> 

			<div class="portfolio-box with-4-col">
				<div class="container">
					
					<ul class="filter">
						
						<div class="#"><li><a href="#" class="active" data-filter="*"><i class="fa fa-th"></i>Mostrar todas</a></li></div>
						<div class="col-md-10"> 
							<li><a href="#" data-filter=".audi">Audi </a></li>
							<li><a href="#" data-filter=".talisman">BMW - MINi&nbsp;&nbsp; </a></li>
							<li><a href="#" data-filter=".manantiales">Cadillac - GMC - Buick&nbsp;&nbsp;</a></li>
							<li><a href="#" data-filter=".chevrolet">Chevrolet&nbsp;&nbsp;</a></li>
							<li><a href="#" data-filter=".chrysler">Chrysler - Dodge - Jeep - RAM&nbsp;&nbsp;</a></li>
							<li><a href="#" data-filter=".honda">&nbsp;&nbsp;Honda&nbsp;&nbsp;</a></li>
							<li><a href="#" data-filter=".isuzu">Isuzu CAMIONES</a></li>
							<li><a href="#" data-filter=".kia">KIA MOTORS</a></li>
							<li><a href="#" data-filter=".fiat">FIAT</a></li>
							<li><a href="#" data-filter=".mitsubishi">Mitsubishi</a></li>
							<li><a href="#" data-filter=".nissan">Nissan</a></li>
							<li><a href="#" data-filter=".toyota">Toyota</a></li>   
							<li><a href="#" data-filter=".vw">Volkswagen</a></li> 
							<li><a href="#" data-filter=".semi">Seminuevos</a></li>   </div>                                         
						</ul>

						<div class="portfolio-container">

							<div class="work-post audi">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/audi.png">
									<div class="hover-box">
										<a class="zoom video" href="audi#quere"></a>
										<a class="page" href="http://www.audicenterqueretaro.com.mx/AC_Home.asp?idc=2150&idS=1" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Audi Center Querétaro</h5>
									<span><a href="http://www.audicenterqueretaro.com.mx/AC_Home.asp?idc=2150&idS=1" target="_blank">Querétaro</a></span>
								</div>
							</div>
							
							<div class="work-post audi">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/audi.png">
									<div class="hover-box">
										<a class="zoom video" href="audi#juri"></a>
										<a class="page" href="http://www.audicenterjuriquilla.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Audi Center Juriquilla</h5>
									<span><a href="http://www.audicenterjuriquilla.com.mx/" target="_blank">Querétaro</a></span>
								</div>
							</div>
							
							
							
							<div class="work-post talisman">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/bmw.png">
									<div class="hover-box">
										<a class="zoom video" href="talisman"></a>
										<a class="page" href="http://fametalisman.com/index.html" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Talismán</h5>
									<span><a href="http://fametalisman.com/" target="_blank">Morelia</a></span>
								</div>										
							</div>
							
							<div class="work-post talisman">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/mini2.png">
									<div class="hover-box">
										<a class="zoom video" href="talisman".png></a>
										<a class="page" href="http://fametalisman.com/index.html" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Talismán</h5>
									<span><a href="http://fametalisman.com/" target="_blank">Morelia</a></span>
								</div>										
							</div>   
							
							<div class="work-post manantiales">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/cadillac.png">
									<div class="hover-box">
										<a class="zoom video" href="gmc"></a>
										<a class="page" href="http://famemanantiales.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Manantiales</h5>
									<span><a href="http://famemanantiales.com/" target="_blank">Morelia</a></span>
								</div>										
							</div>   
							
							<div class="work-post manantiales">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/gmc.png">
									<div class="hover-box">
										<a class="zoom video" href="gmc"></a>
										<a class="page" href="http://famemanantiales.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Manantiales</h5>
									<span><a href="http://famemanantiales.com/" target="_blank">Morelia</a></span>
								</div>										
							</div>  
							
							<div class="work-post manantiales">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/buick.png">
									<div class="hover-box">
										<a class="zoom video" href="gmc"></a>
										<a class="page" href="http://famemanantiales.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Manantiales</h5>
									<span><a href="http://famemanantiales.com/" target="_blank">Morelia</a></span>
								</div>										
							</div> 
							
							
							
							<div class="work-post chevrolet">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/chevrolet.png">
									<div class="hover-box">
										<a class="zoom video" href="chevrolet#chevmorelia"></a>
										<a class="page" href="http://famemorelia.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chevrolet</h5>
									<span><a href="http://famemorelia.com/" target="_blank">FAME Morelia</a></span>
								</div>										
							</div>     
							
							<div class="work-post chevrolet">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/chevrolet.png">
									<div class="hover-box">
										<a class="zoom video" href="chevrolet#chevapatz"></a>
										<a class="page" href="http://chevroletapatzingan.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chevrolet</h5>
									<span><a href="http://chevroletapatzingan.com/" target="_blank">FAME Apatzingán</a></span>
								</div>										
							</div>                                                                                     
							<div class="work-post chevrolet">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/chevrolet.png">
									<div class="hover-box">
										<a class="zoom video" href="chevrolet#chevuru"></a>
										<a class="page" href="http://famecupatitzio.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chevrolet</h5>
									<span><a href="http://famecupatitzio.com" target="_blank">FAME Cupatitzio Uruapan</a></span>
								</div>										
							</div>                        
							

							<div class="work-post chevrolet">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/chevrolet.png">
									<div class="hover-box">
										<a class="zoom video" href="chevrolet#chevlaz"></a>
										<a class="page" href="http://famelazarocardenas.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chevrolet</h5>
									<span><a href="http://famelazarocardenas.com/" target="_blank">Lázaro Cárdenas</a></span>
								</div>										
							</div>     
							
							<div class="work-post chevrolet">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/chevrolet.png">
									<div class="hover-box">
										<a class="zoom video" href="chevrolet#chevsah"></a>
										<a class="page" href="http://chevroletsahuayo.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chevrolet</h5>
									<span><a href="http://chevroletsahuayo.com/" target="_blank">FAME Sahuayo</a></span>
								</div>										
							</div>     

							<div class="work-post chevrolet">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/chevrolet.png">
									<div class="hover-box">
										<a class="zoom video" href="chevrolet#chevzam"></a>
										<a class="page" href="http://famezamora.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chevrolet</h5>
									<span><a href="http://famezamora.com/" target="_blank">FAME Zamora</a></span>
								</div>										
							</div>  
							
							<div class="work-post chrysler">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/chrysler.png">
									<div class="hover-box">
										<a class="zoom video" href="chrysler#chrysuru"></a>
										<a class="page" href="http://famemedina.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chrysler</h5>
									<span><a href="http://famemedina.com/" target="_blank">FAME Uruapan</a></span>
								</div>										
							</div>    
							
							<div class="work-post chrysler">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/chrysler.png">
									<div class="hover-box">
										<a class="zoom video" href="chrysler#chrysqro"></a>
										<a class="page" href="http://www.famequeretaro.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chrysler</h5>
									<span><a href="http://www.famequeretaro.com.mx/" target="_blank">FAME Querétaro</a></span>
								</div>										
							</div>               
							
							<div class="work-post chrysler">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/dodge.png">
									<div class="hover-box">
										<a class="zoom video" href="chrysler#chrysuru"></a>
										<a class="page" href="http://famemedina.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chrysler</h5>
									<span><a href="http://famemedina.com/" target="_blank">FAME Uruapan</a></span>
								</div>										
							</div>    
							
							<div class="work-post chrysler">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/dodge.png">
									<div class="hover-box">
										<a class="zoom video" href="chrysler#chrysqro"></a>
										<a class="page" href="http://www.famequeretaro.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Chrysler</h5>
									<span><a href="http://www.famequeretaro.com.mx/" target="_blank">FAME Querétaro</a></span>
								</div>										
							</div>      
							
							<div class="work-post fiat">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/fiat.png">
									<div class="hover-box">
										<a class="zoom video" href="fiat"></a>
										<a class="page" href="http://fiaturuapanfcamx.cms.dealer.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>FIAT</h5>
									<span><a href="http://fiaturuapanfcamx.cms.dealer.com/" target="_blank">FAME Uruapan</a></span>
								</div>
							</div>


							<div class="work-post fiat">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/fiat.png">
									<div class="hover-box">
										<a class="zoom video" href="fiat"></a>
										<a class="page" href="http://fiatqueretarofcamx.cms.dealer.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>FIAT</h5>
									<span><a href="http://fiatqueretarofcamx.cms.dealer.com/" target="_blank">FAME Querétaro</a></span>
								</div>
							</div> 
							
							<div class="work-post mitsubishi">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/mitsubishi.png">
									<div class="hover-box">
										<a class="zoom video" href="mitsubishi#queretaro"></a>
										<a class="page" href="http://www.mitsubishiqueretaro.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Mitsubishi</h5>
									<span><a href="http://www.mitsubishiqueretaro.com.mx/" target="_blank">Querétaro</a></span>
								</div>										
							</div> 
							
							<div class="work-post mitsubishi">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/mitsubishi.png">
									<div class="hover-box">
										<a class="zoom video" href="mitsubishi#uruapan"></a>
										<a class="page" href="http://www.mitsubishiuruapan.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Mitsubishi</h5>
									<span><a href="http://www.mitsubishiuruapan.com.mx/" target="_blank">Uruapan</a></span>
								</div>										
							</div>   

							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#feb"></a>
										<a class="page" href="http://honda5defebrero.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda 5 de Febrero</h5>
									<span><a href="http://honda5defebrero.com/" target="_blank">Querétaro</a></span>
								</div>										
							</div>    
							
							
							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#atizapan"></a>
										<a class="page" href="http://hondaatizapan.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda Atizapán</h5>
									<span><a href="http://hondaatizapan.com/" target="_blank">Ciudad de México</a></span>
								</div>										
							</div> 
							
							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#altozano"></a>
										<a class="page" href="http://hondaaltozano.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda Altozano</h5>
									<span><a href="http://hondaaltozano.com/" target="_blank"> Morelia</a></span>
								</div>										
							</div>                        

							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#manantiales"></a>
										<a class="page" href="http:http://hondamanantiales.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda Manantiales</h5>
									<span><a href="http://hondamanantiales.com/" target="_blank">Morelia</a></span>
								</div>										
							</div>

							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#corregidora"></a>
										<a class="page" href="http://famecorregidora.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda Corregidora</h5>
									<span><a href="http://famecorregidora.com/" target="_blank">Querétaro</a></span>
								</div>										
							</div>
							
							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#marquesa"></a>
										<a class="page" href="http://hondamarquesa.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda Marquesa</h5>
									<span><a href="http://hondamarquesa.com/" target="_blank">Querétaro</a></span>
								</div>										
							</div>
							
							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#monarca"></a>
										<a class="page" href="http://famemonarca.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda Monarca</h5>
									<span><a href="http://famemonarca.com/" target="_blank">Morelia</a></span>
								</div>										
							</div>  
							
							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#monarcadf"></a>
										<a class="page" href="http://famemonarcadf.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda Monarca D.F</h5>
									<span><a href="http://famemonarcadf.com/" target="_blank">Cuidad de México</a></span>
								</div>										
							</div>                                                                       

							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#sanjuan"></a>
										<a class="page" href="http://hondasanjuandelrio.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda San Juan del Río</h5>
									<span><a href="http://hondasanjuandelrio.com/" target="_blank">Querétaro</a></span>
								</div>										
							</div>
							
							<div class="work-post honda">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/honda.png">
									<div class="hover-box">
										<a class="zoom video" href="honda#uruapan"></a>
										<a class="page" href="http://hdu.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Honda Uruapan</h5>
									<span><a href="http://hdu.com.mx/" target="_blank">Michoacán</a></span>
								</div>										
							</div>     
							
							<div class="work-post isuzu">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/isuzu.png">
									<div class="hover-box">
										<a class="zoom video" href="honda"></a>
										<a class="page" href="http://famecamiones.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Isuzu Fame Camiones</h5>
									<span><a href="http://famecamiones.com/" target="_blank">Morelia</a></span>
								</div>										
							</div>  
							
							<div class="work-post kia">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/agencias/kia.png">
									<div class="hover-box">
										<a class="zoom video" href="kia#pedregal"></a>
										<a class="page" href="http://kiapedregal.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>KIA Motors</h5>
									<span><a href="http://kiapedregal.com/" target="_blank">FAME Pedregal</a></span>
								</div>										
							</div>  
							
							<div class="work-post kia">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/agencias/kia-delduero.png">
									<div class="hover-box">
										<a class="zoom video" href="kia#duero"></a>
										<a class="page" href="http://kiadelduero.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>KIA Motors</h5>
									<span><a href="http://kiadelduero.com/" target="_blank">FAME Del Duero</a></span>
								</div>										
							</div>    
							
							<div class="work-post kia">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/agencias/kia-paricutin.png">
									<div class="hover-box">
										<a class="zoom video" href="kia#paricutin"></a>
										<a class="page" href="http://kiaparicutin.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>KIA Motors</h5>
									<span><a href="http://kiaparicutin.com/" target="_blank">FAME Paricutín</a></span>
								</div>										
							</div>  
							
							
							
							<div class="work-post kia">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/agencias/kia-milcumbres.png">
									<div class="hover-box">
										<a class="zoom video" href="kia#mil"></a>
										<a class="page" href="http://www.kiamilcumbres.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>KIA Motors</h5>
									<span><a href="http://www.kiamilcumbres.com/" target="_blank">FAME Milcumbres</a></span>
								</div>									
							</div>    
							
							
							<div class="work-post chrysler">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/jeep.png">
									<div class="hover-box">
										<a class="zoom video" href="chrysler#chrysuru"></a>
										<a class="page" href="http://famemedina.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Jeep</h5>
									<span><a href="http://famemedina.com/" target="_blank">FAME Uruapan</a></span>
								</div>										
							</div>    
							
							<div class="work-post chrysler">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/jeep.png">
									<div class="hover-box">
										<a class="zoom video" href="chrysler#chrysqro"></a>
										<a class="page" href="http://www.famequeretaro.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Jeep</h5>
									<span><a href="http://www.famequeretaro.com.mx/" target="_blank">FAME Querétaro</a></span>
								</div>										
							</div>      
							
							
							<div class="work-post nissan">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/nissan.png">
									<div class="hover-box">
										<a class="zoom video" href="nissan"></a>
										<a class="page" href="http://nissanacueducto.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Nissan</h5>
									<span><a href="http://nissanacueducto.com.mx/" target="_blank">Acueducto Morelia</a></span>
								</div>										
							</div>
							
							
							
							<div class="work-post chrysler">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/ram.png">
									<div class="hover-box">
										<a class="zoom video" href="chrysler#chrysuru"></a>
										<a class="page" href="http://famemedina.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>RAM</h5>
									<span><a href="http://famemedina.com/" target="_blank">FAME Uruapan</a></span>
								</div>										
							</div>    
							
							<div class="work-post chrysler">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/ram.png">
									<div class="hover-box">
										<a class="zoom video" href="chrysler#chrysqro"></a>
										<a class="page" href="http://www.famequeretaro.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>RAM</h5>
									<span><a href="http://www.famequeretaro.com.mx/" target="_blank">FAME Querétaro</a></span>
								</div>										
							</div>       
							
							<div class="work-post toyota">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/toyota.png">
									<div class="hover-box">
										<a class="zoom video" href="toyota#valladolid"></a>
										<a class="page" href="http://www.famevalladolid.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Toyota Morelia</h5>
									<span><a href="http://www.famevalladolid.com/" target="_blank">Valladolid</a></span>
								</div>										
							</div> 
							
							<div class="work-post toyota">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/toyota.png">
									<div class="hover-box">
										<a class="zoom video" href="toyota#perisur"></a>
										<a class="page" href="http://www.fameperisur.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Toyota México</h5>
									<span><a href="http://www.fameperisur.com/" target="_blank">Perisur</a></span>
								</div>										
							</div>

							<div class="work-post toyota">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/toyota.png">
									<div class="hover-box">
										<a class="zoom video" href="toyota#uruapan"></a>
										<a class="page" href="http://www.toyotauruapan.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Toyota Uruapan</h5>
									<span><a href="http://www.toyotauruapan.com/" target="_blank">Uruapan</a></span>
								</div>										
							</div>

								<div class="work-post toyota">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/toyota.png">
									<div class="hover-box">
										<a class="zoom video" href="toyota#lapiedad"></a>
										<a class="page" href="http://www.toyotalapiedad.com/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Toyota La Piedad</h5>
									<span><a href="http://www.toyotalapiedad.com/" target="_blank">La Piedad</a></span>
								</div>										
							</div>
							
							<div class="work-post vw">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/vw.png">
									<div class="hover-box">
										<a class="zoom video" href="vw"></a>
										<a class="page" href="http://www.vw-fame.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Volskwagen </h5>
									<span><a href="http://www.vw-fame.com.mx/" target="_blank">Morelia</a></span>
								</div>										
							</div>  
							
							<div class="work-post semi">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/daswelt.png">
									<div class="hover-box">
										<a class="zoom video" href="daswelt"></a>
										<a class="page" href="http://www.vw-fame.com.mx/" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Das WeltAuto </h5>
									<span><a href="http://www.vw-fame.com.mx/" target="_blank">Morelia</a></span>
								</div>										
							</div> 
							
							<div class="work-post semi">
								<div class="work-post-gal">
									<img alt="" src="../assets/images/s-agencias/seminuevos.png">
									<div class="hover-box">
										<a class="zoom video" href="http://fameseminuevos.com/index.php/agencia/agencia/1"></a>
										<a class="page" href="http://fameseminuevos.com/index.php/agencia/agencia/1" target="_blank"></a>
									</div>
								</div>
								<div class="work-post-content">
									<h5>Seminuevos FAME</h5>
									<span><a href="http://fameseminuevos.com/index.php/agencia/agencia/1" target="_blank">Morelia</a></span>
								</div>										
							</div> 
							
						</div>
					</div>
				</div>
			</div>
			<!-- End content -->

			
			<?php include('../assets/contenido/footer.php'); ?>
			
			<script type="text/javascript" src="contenido/script.js"></script>

			<script src="https://use.fontawesome.com/b93ad5b4e6.js"></script>
			<script type="text/javascript" src="js/jquery.min.js"></script>
			<script type="text/javascript" src="js/jquery.migrate.js"></script>
			<script type="text/javascript" src="js/jquery.magnific-popup.min.js"></script>
			<script type="text/javascript" src="js/bootstrap.js"></script>
			<script type="text/javascript" src="js/jquery.imagesloaded.min.js"></script>
			<script type="text/javascript" src="js/jquery.isotope.min.js"></script>
			<script type="text/javascript" src="js/retina-1.1.0.min.js"></script>
			<script type="text/javascript" src="js/plugins-scroll.js"></script>
			<script type="text/javascript" src="js/script.js"></script>

