<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Chrysler, Dodge, Jeep y RAM</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headerchrysler.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Chrysler, Dodge, Jeep y RAM</h2>
			</div>
		</div>

		<!-- blog-box Banner -->
		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						<div class="single-project-content">
							<!--								<img alt="" src="upload/single-project.jpg"> -->


							<div class="mfp-iframe-holder">
								<div class="col-md-4"><h2>FAME Chrysler Querétaro</h2>
									<p>Te esperamos con gusto en: <br>
										<strong>Av. Luís M. Vega #302 Col. Ampliación Cimatario<br>
										Querétaro, Querétaro, C.P. 76030 Teléfono: 442 2519400</strong></p>
										<p><a href="http://www.famequeretaro.com.mx/CHRYSLER/" target="_blank"><strong>www.famequeretaro.com</strong></a></p>

									</div>
										<div class="col-md-8">
											<a name="chrysqro"></a>

											<iframe width='100%' height='500px' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3140.97066636541!2d-100.39034802372866!3d20.57724655562712!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xf6ce86ace286a9ec!2sCHRYSLER%2C+DODGE%2C+JEEP+Y+RAM+Fame+Automotriz+Quer%C3%A9taro!5e0!3m2!1ses-419!2smx!4v1518130599326"   frameborder="0" ></iframe>

											<small><a target="_blank" href="https://goo.gl/maps/NId4X">Ver en Street View</a></small>
										</div>

									</div>


								</div>
							</div>
							<img src="../../assets/images/shadow.png" style="width:100%;">
							<div class="col-md-12">


								<div class="single-project-content">
									<div class="mfp-iframe-holder">
										<div class="col-md-4"><h2>FAME Chrysler Uruapan</h2>
											<p>Te esperamos con gusto en: <br>
												<strong>Prol. Paseo Lázaro Cárdenas #500. Fracc. Jardines del Bosque<br>
													Uruapan, Michoacán. C.P. 60190<br> Teléfono: (452) 527 5700</strong></p>
													<p><a href="http://www.medina-automotriz.com.mx/" target="_blank"><strong>www.medina-automotriz.com.mx</strong></a></p>
												</div> 
												<div class="col-md-8">
													<a name="chrysuru"></a><small>
														<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1881.8573959434998!2d-102.0593086441803!3d19.381498329184723!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de2979740f9b3%3A0xcdb3e002a6077b3f!2sFAME+Medina+Uruapan+(Chrysler%2C+Dodge%2C+RAM%2C+Jeep)!5e0!3m2!1ses!2smx!4v1416943607003"></iframe>

														<a href="https://goo.gl/maps/js07z">Ver en Street View</a></small>
													</div></div>

												</div>                         

											</div>
											<img src="../../assets/images/shadow.png" style="width:100%;">

										</div>

									</div>
								</div>

							</div>
							<!-- End content -->


							<?php include('../../assets/contenido/agencias/footer.php'); ?>
						</body>