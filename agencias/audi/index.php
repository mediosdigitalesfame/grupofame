<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - AUDI</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headeraudi.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Audi</h2>
			</div>
		</div>
		
		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">

						<div class="single-project-content">
							<div class="mfp-iframe-holder">

								<div id="quere" class="col-md-4">
									<h2>Audi Center Querétaro</h2><br>
									<p>
										<strong>Te esperamos con gusto en: </strong><br>
										Prol. Av. Luis M. Vega No. 302-B. Colonia Ampliación Cimatario.
										Querétaro, Qro. C.P. 76030.<br><br>
										<span><i class="fa fa-phone"></i></span> 
										<strong>Teléfono: (442) 251 9813</strong>
									</p>
									<p> 
										<a href="http://www.audicenterqueretaro.com.mx/AC_Home.asp?idc=2150&idS=1" target="_blank"><strong> www.audicenterqueretaro.com</strong></a>
									</p>
									<div class="col-md-12">
										<img src="../../assets/images/s-agencias/audi.png">
									</div>
								</div>
								
								
								<div class="col-md-8">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3735.2809049138737!2d-100.39242168459816!3d20.576582986245064!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d344d9e5656187%3A0xd60fe1df800a7027!2sFAME+-+AUDI+Queretaro!5e0!3m2!1ses!2smx!4v1538671027117" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>
									

									<small><a target="_blank" href="https://goo.gl/maps/dl51K" style="color:#848484;text-align:left">Ver en Street View</a></small>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<img src="../../assets/images/shadow.png" style="width:100%;">

		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">

						<div class="single-project-content">
							<div class="mfp-iframe-holder">
								<div class="col-md-4">
									<h2>Audi Center Juriquilla</h2><br>
									<p>
										<strong>Te esperamos con gusto en:</strong> <br>
										Anillo Vial Fray Junípero Serra #17512, San Pedrito el Alto. 76085 Querétaro, QRO<br><br>
										<span><i class="fa fa-phone"></i></span> <strong>Telefono: 01 442 402 1710</strong>
									</p>
									<p>
										<a href="http://audicenterjuriquilla.com.mx/" target="_blank"><strong>www.audicenterjuriquilla.com</strong></a>
									</p>
									<div class="col-md-12"><img src="../../assets/images/s-agencias/audi.png"></div>
								</div>
								
								<div class="col-md-8">
									<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3733.075823578709!2d-100.39957568459693!3d20.666493986195785!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d35a3374ec44f9%3A0x92e7646c22fa58ad!2sAudi+Center+Juriquilla!5e0!3m2!1ses!2smx!4v1538671137231" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

									<small><a target="_blank" href="https://goo.gl/maps/dl51K" style="color:#848484;text-align:left">Ver en Street View</a></small>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
		
		<?php include('../../assets/contenido/agencias/footer.php'); ?>