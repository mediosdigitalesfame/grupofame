<!doctype html>

<html lang="es" class="no-js">

<head>



	<!-- Global site tag (gtag.js) - Google Analytics -->

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>

<script>

  window.dataLayer = window.dataLayer || [];

  function gtag(){dataLayer.push(arguments);}

  gtag('js', new Date());



  gtag('config', 'UA-91159946-1');

</script>





	<title>Grupo FAME - Chevrolet</title>

	<?php include('contenido/head.php'); ?>

</head>    

<body>

	<div id="container">

		

		<?php include('../../assets/contenido/agencias/headerchevrolet.php'); ?>



		<div class="page-banner">

			<div class="container">

				<h2>Chevrolet</h2>

			</div>

		</div>

		

		<div class="single-project-page">

			<div class="container">

				<div class="row">

					

					<div class="col-md-12">

						<div class="single-project-content">



							<div class="mfp-iframe-holder">

								<div class="col-md-4" id="chevmorelia">

									

									

									<h2>FAME Chevrolet Morelia <br> </h2> 

									

									

									<p><strong> Te esperamos con gusto en: </strong><br>

										Periférico Paseo de la República #551 Colonia Los Manantiales | Morelia, Michoacán. C.P. 58186 <br><br>

										<span><i class="fa fa-phone"></i></span> <strong>Teléfono: (443) 298 0755</strong><br>

										<span><i class="fa fa-envelope"></i></span><strong><a href="http://www.famechevrolet.com.mx/" target="_blank"> www.chevroletmorelia.com</a></strong>

									</p>

									<div class="col-md-12">

										<img src="../../assets/images/s-agencias/chevrolet.png">

									</div>

								</div>



								<div class="col-md-8">

									



									<a name="atizapan"></a>





									<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1878.2571084130755!2d-101.23835828697793!3d19.690719931253295!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0ea7f2ac343d%3A0x9f31ba0a91199a41!2sFame+Morelia%2C+S.A.+de+C.V.!5e0!3m2!1ses!2smx!4v1416871511974"></iframe>

									

									<small><a target="_blank" href="https://www.google.com.mx/maps/@19.54973,-99.2761112,3a,75y,255.97h,94.35t/data=!3m4!1e1!3m2!1sW9auy6QUM4DbakcgHeYJNQ!2e0!6m1!1e1" style="color:#848484;text-align:left">Ver en Street View</a></small>



								</div>

							</div>

						</div>

					</div>

				</div>

			</div>

			<img src="../../assets/images/shadow.png" style="width:100%;">

			

			<div class="col-md-12">            

				

				<div class="single-project-page">

					<div class="container">

						<div class="row">

							<div class="col-md-12">

								<div class="col-md-4" id="chevapatz">

									<div class="single-project-content">

										

										<br><br><br><h2>FAME Chevrolet Apatzingán <br> </h2> 

										<h3></h3><br>

										

										<p>Te esperamos con gusto en: <br>

											<strong>Carretera Apatzingán - Uruapan Kilómetro 1.5   ·   Col. Palmira<br>

												Apatzingán, Michoacán. C.P. 60698  <br> Teléfono: (453) 534 0237</strong></p>

												

												<p><a href="http://www.chevroletapatzingan.com/" target="_blank"><strong>www.chevroletapatzingan.com</strong></a></p>



												<p><div class="col-md-10"><img src="../../assets/images/s-agencias/chevrolet.png"></div>



												</div>

											</div>

											

											<div class="col-md-8">

												<div class="mfp-iframe-holder">



													<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1885.3979768500394!2d-102.34451557082342!3d19.072706892220342!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8431e3e23e5e47cd%3A0x1b669ca26a77ff89!2sFAME+Chevrolet+Apatzing%C3%A1n!5e0!3m2!1ses!2smx!4v1416870296589"></iframe>

													<a name="atizapan"></a>

													<small><a target="_blank" href="https://www.google.com.mx/maps/@19.54973,-99.2761112,3a,75y,255.97h,94.35t/data=!3m4!1e1!3m2!1sW9auy6QUM4DbakcgHeYJNQ!2e0!6m1!1e1" style="color:#848484;text-align:left">Ver en Street View</a></small>



												</div>

											</div>

										</div>

									</div>

								</div>

							</div>

							<img src="../../assets/images/shadow.png" style="width:100%;">

							

							<div class="col-md-12">

								

								<div class="single-project-page">

									<div class="container">

										<div class="row">

											<div class="col-md-12">

												<div class="col-md-4" id="chevuru">

													<div class="single-project-content">

														

														<br><br><br><h2>FAME Chevrolet Cupatitzio Uruapan <br> </h2> 

														<h3></h3><br>

														

														

														<p>Te esperamos con gusto en: <br>

															<strong>Av. Prol . Lázaro Cárdenas No. 3591, Fracc. Jardines del Bosque<br>

																Uruapan, Michoacán. C.P. 60190 <br>Teléfono: (452) 503 3900</strong></p>

																<p><a href="http://www.famecupatitzio.com/" target="_blank"><strong>www.famecupatitzio.com</strong></a></p>



																<p><div class="col-md-10"><img src="../../assets/images/s-agencias/chevrolet.png"></div></p>



															</div>

														</div>

														

														<div class="col-md-8">

															<div class="mfp-iframe-holder">



																<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d1881.8552211857132!2d-102.0591581876416!3d19.381686545818567!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1schevrolet+cerca+de+Uruapan!5e0!3m2!1ses!2smx!4v1416874910243"></iframe>



																<small><a target="_blank" href="https://www.google.com.mx/maps/@19.3815517,-102.0594274,3a,75y,96.8h,91.58t/data=!3m6!1e1!3m4!1sG5pcGGE_aa1ALIa8_5jgBg!2e0!7i13312!8i6656" style="color:#848484;text-align:left">Ver en Street View</a></small>



															</div>

														</div>

													</div>

												</div>

											</div>

										</div>

										<img src="../../assets/images/shadow.png" style="width:100%;">

										

										<div class="col-md-12">

											

											<div class="single-project-page">

												<div class="container">

													<div class="row">

														<div class="col-md-12">

															<div class="col-md-4" id="chevlaz">

																<div class="single-project-content">

																	

																	<br><br><br><h2>FAME Chevrolet Lázaro Cárdenas</h2>                           

																	<p>Te esperamos con gusto en: <br>

																		<strong>Av. Las Palmas #3198<br> Lázaro Cárdenas, Michoacán. <br>Teléfono: (753) 537 1898</strong></p>

																		<p><a href="http://www.famelazarocardenas.com/" target="_blank"><strong>www.famelazarocardenas.com</strong></a></p>



																		<p><div class="col-md-10"><img src="../../assets/images/s-agencias/chevrolet.png"></div></p>



																	</div>

																</div>

																

																<div class="col-md-8">

																	<div class="mfp-iframe-holder">



																		<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1897.6523203175082!2d-102.20051231278413!3d17.964558275723185!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x84315c66f39a4a4d%3A0xba32aa50c7b731ff!2sFAME+Chevrolet+Lazaro+Cardenas!5e0!3m2!1ses!2smx!4v1416875724024"></iframe>



																	</div>

																</div>

															</div>

														</div>

													</div>

												</div>

												<img src="../../assets/images/shadow.png" style="width:100%;">

												

												<div class="col-md-12">

													

													<div class="single-project-page">

														<div class="container">

															<div class="row">

																<div class="col-md-12">

																	<div class="col-md-4" id="chevsah">

																		<div class="single-project-content">

																			

																			<br><br><br><h2>FAME Chevrolet Sahuayo</h2>                           

																			<p>Te esperamos con gusto en: <br>

																				<strong>Blvd. Lázaro Cárdenas s/n<br>

																					Sahuayo, Michoacán. C.P. 59000<br> Teléfono: (353) 532 0378</strong></p>

																					<p><a href="http://chevroletsahuayo.com/" target="_blank"><strong>www.chevroletsahuayo.com</strong></a></p>



																					<p><div class="col-md-10"><img src="../../assets/images/s-agencias/chevrolet.png"></div></p>



																				</div>

																			</div>

																			

																			<div class="col-md-8">

																				<div class="mfp-iframe-holder">



																					<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3747.681349040442!2d-102.71577291401677!3d20.063785487266628!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842ee3544a748ce1%3A0x9194dc33468c552f!2sFAME+Chevrolet+Sahuayo!5e0!3m2!1ses!2smx!4v1416933357706"></iframe>

																					

																					<small><a target="_blank" href="https://www.google.com/maps/@20.0634072,-102.7163387,3a,75y,267.99h,96.93t/data=!3m6!1e1!3m4!1s364dgU_4tSFBIZQy39Oxtg!2e0!7i13312!8i6656" style="color:#848484;text-align:left">Ver en Street View</a></small>



																				</div>

																			</div>

																		</div>

																	</div>

																</div>

															</div>

															<img src="../../assets/images/shadow.png" style="width:100%;">

															

															<div class="col-md-12">

																

																<div class="single-project-page">

																	<div class="container">

																		<div class="row">

																			<div class="col-md-12">

																				<div class="col-md-4" id="chevzam">

																					<div class="single-project-content">

																						

																						<br><br><br><h2>FAME Chevrolet Zamora</h2>                           

																						<p>Te esperamos con gusto en: <br>

																							<strong>Fray. Manuel Martínez de Navarrete #5 <br>

																								Col.Centro C.P. 59600 Zamora, Michoacán <br> Teléfono:  (351) 512 1640</strong></p>

																								<p><a href="http://famezamora.com/" target="_blank"><strong>www.famezamora.com</strong></a></p>



																								<p><div class="col-md-10"><img src="../../assets/images/s-agencias/chevrolet.png"></div></p>



																							</div>

																						</div>

																						

																						<div class="col-md-8">

																							<div class="mfp-iframe-holder">



																								<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1874.8422060791886!2d-102.28967701717839!3d19.979770022172968!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842e88db36b737f5%3A0x93dbabe2fc334703!2sFAME+Chevrolet+Zamora!5e0!3m2!1ses!2smx!4v1416933701747"></iframe>

																								

																								<small><a target="_blank" href="https://www.google.com/maps/@19.9798515,-102.2889284,3a,75y,185.73h,93.98t/data=!3m6!1e1!3m4!1sy9RZoAjly3rpN_S5HK8gVg!2e0!7i13312!8i6656" style="color:#848484;text-align:left">Ver en Street View</a></small>



																							</div>

																						</div>

																					</div>

																				</div>

																			</div>

																		</div>

																		<img src="../../assets/images/shadow.png" style="width:100%;">

																		

																	</div>



																</div>



															</div>

														</div>



														

														

														<?php include('../../assets/contenido/agencias/footer.php'); ?>