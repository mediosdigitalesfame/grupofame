<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Chevrolet Sahuayo</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerchevroletcontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Chevrolet Sahuayo</h2>
			</div>
		</div>


			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								        <?php include('../../../assets/formularios/formchevsahu.php'); ?>
                                     </div>
                                 </div>
                             </div>
                        
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Blvd. Lázaro Cárdenas S/N</span> <span>C.P. 59000</span> <span>Sahuayo, Michoacán</span></li>
									<li><span><i class="fa fa-phone"></i><strong>(353) 532 0378</strong></span></li>
                                    
                                    <h3>Whatsapp</h3>
                               <li><span><i class="fa fa-whatsapp"></i><strong>  Ventas   | 3531050482 </strong></span></li><strong></strong> 

                               <h3>Whatsapp</h3>
                               <li><span><i class="fa fa-whatsapp"></i><strong>  Citas de Servicio <br>4432732794 | 4433771717</strong></span></li>
                               
		</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Chevrolet FAME</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>
							</div>
						</div>
                
					</div>
				</div>
			</div>

		</div> 

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>