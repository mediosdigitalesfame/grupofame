<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Chevrolet Zamora</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerchevroletcontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Chevrolet Zamora</h2>
			</div>
		</div>

			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('../../../assets/formularios/formchevroletzamorapostventa.php'); ?>
                                     </div>
                                 </div>
                             </div>
                        
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Fray. Manuel Martínez de Navarrete #579 Col.Centro  <span>C.P. 59600</span> <span>Zamora, Michoacán</span></li>
									<li><span><i class="fa fa-phone"></i><strong>(351) 512 1640</strong></span></li>
                                    
                                   <li>
                                    <i class="fa fa-phone"></i><span>Recepción <strong>Ext. 101</strong></span><br>                              
                                   <i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 221</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Postventa <strong>Ext. 106</strong></span><br>
									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 218</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong>Ext. 107, 109, 222 y 224</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Gerencia <strong>Ext. 105</strong></span><br>              

                                    <i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 118</strong></span><br>                             
                                    </li>
								
                                
                                <h3>Whatsapp</h3>
                       				<li><span><i class="fa fa-whatsapp"></i><strong> Ventas y Postventa <br> 3511350656  |  3511077722 </strong></span></li>

                       				<h3>Whatsapp</h3>
                               <li><span><i class="fa fa-whatsapp"></i><strong>  Citas de Servicio <br>4432732794 | 4433771717</strong></span></li>
                               
							</ul>
                            </div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Chevrolet FAME</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>
							</div>
						</div>
                        
					</div>
				</div>
			</div>

		</div> 

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>