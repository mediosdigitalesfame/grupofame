<!doctype html>
<html lang="es" class="no-js">
<head>
	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Chevrolet Uruapan</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerchevroletcontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Chevrolet Uruapan</h2>
			</div>
		</div>

			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								         <?php include('../../../assets/formularios/formfamecupat.php'); ?>
                                     </div>
                                 </div>
                             </div>
                        
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Av. Prolongación Lázaro Cárdenas #3591</span> <span>Fracc. Jardines del Bosque</span><span>C.P. 60190</span> <span>Uruapan, Michoacán</span></li>
									<li><span><i class="fa fa-phone"></i><strong>(452) 503 3900</strong></span></li>
                                   <li><i class="fa fa-phone"></i><span>Taller de Servicio <strong>Ext. 127</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Postventa <strong>Ext. 112</strong></span><br>
									<i class="fa fa-phone"></i><span>Refacciones <strong>Ext. 112</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong>Ext. 103</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Seminuevos <strong>Ext. 109</strong></span><br>
                                    <i class="fa fa-phone"></i><span>Recepción <strong>Ext. 101</strong></span><br>  

                                    <i class="fa fa-phone"></i><span>Financiamiento <strong>Ext. 117</strong></span><br>                             
                                    </li>
								<p>
                                <h3>Whatsapp</h3>
                               <li><span><i class="fa fa-whatsapp"></i><strong>  Ventas   y  Postventa <br>4521492878 | 4521000646</strong></span></li></p>

                               <h3>Whatsapp</h3>
                               <li><span><i class="fa fa-whatsapp"></i><strong>  Citas de Servicio <br>4432732794 | 4433771717</strong></span></li>

                               <strong></strong>
                                </ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Chevrolet FAME</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>
                                
							</div>
						</div>
                    
					</div>
				</div>
			</div>

		</div> 

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>