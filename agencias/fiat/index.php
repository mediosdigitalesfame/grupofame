<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Fiat</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headerfiat.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Fiat</h2>
			</div>
		</div>

		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						<div class="col-md-4">
							<div class="single-project-content">
								<!--								<img alt="" src="upload/single-project.jpg"> -->
								<br><br><br><h2>FIAT FAME Querétaro</h2><br>
								<p><strong>Te esperamos con gusto en: </strong><br>
									Prol. Av. Luis M. Vega No. 302-B. Colonia Ampliación Cimatario.
									Querétaro, Qro. C.P. 76030.<br><br>
									<span><i class="fa fa-phone"></i></span> <strong>Teléfono: (442) 251 9400</strong>
								</p>
								<p><a href="http://fiatfame.com/" target="_blank"><strong>www.fiatfame.com</strong></a>
									<div class="col-md-10"><img src="../../assets/images/s-agencias/fiat.png"></div>
								</p>
							</div></div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">

									<iframe width='100%' height='500px' frameBorder='0'  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8883.881972898407!2d-100.39308011746172!3d20.579387927122898!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x85d344d9f8f8578d%3A0xe231b81a1061ede2!2sCalle+Luis+M.+Vega+32A%2C+zona+dos+extendida%2C+Cimatario%2C+76030+Santiago+de+Quer%C3%A9taro%2C+Qro.!5e0!3m2!1ses-419!2smx!4v1480984589451"></iframe>

									<small><a target="_blank" href="https://goo.gl/maps/3pnsYx6ropE2" style="color:#848484;text-align:left">Ver en Street View</a></small>

								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<img src="../../assets/images/shadow.png" style="width:100%;">
			<div class="single-project-page">
				<div class="container">
					<div class="row">

						<div class="col-md-12">
							<div class="col-md-4">
								<div class="single-project-content">
									<!--								<img alt="" src="upload/single-project.jpg"> -->
									<br><br><br><h2>FIAT FAME Uruapan</h2><br>
									<p><strong>Te esperamos con gusto en:</strong> <br>
										Prol. Paseo Lázaro Cárdenas #500. Fracc. Jardines del Bosque<br><br>
										<span><i class="fa fa-phone"></i></span> <strong>Telefono:(452) 52 75700</strong>
									</p>
									<p><a href="http://fiaturuapan.com/" target="_blank"><strong>www.fiaturuapan.com</strong></a>
										<div class="col-md-10"><img src="../../assets/images/s-agencias/fiat.png"></div>
									</p>

								</div></div>

								<div class="col-md-8">
									<div class="mfp-iframe-holder">

										<iframe width='100%' height='500px' frameBorder='0' src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5322.722182450436!2d-102.06091282201638!3d19.380712572037726!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x1ef3fd349bf61c18!2sCHRYSLER%2C+DODGE%2C+JEEP+y+RAM+FAME+Medina+Uruapan!5e0!3m2!1ses-419!2smx!4v1481050440037"></iframe>

										<small><a target="_blank" href="https://goo.gl/maps/btUifvNp3iD2" style="color:#848484;text-align:left">Ver en Street View</a></small>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- End content -->

		<?php include('../../assets/contenido/agencias/footer.php'); ?>