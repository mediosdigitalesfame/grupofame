<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - BMW-MINI Talisman</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headertalismancontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - BMW-MINI Talisman</h2>
			</div>
		</div>
 
	 <div class="contact-box">
		 <div class="container">
			 <div class="row">

				 <div class="col-md-6" align="center">
                     <div class="container">
					     <div class="col-md-12" >
							<?php include('../../../assets/formularios/formbmw.php'); ?>
                         </div>
                     </div>
                 </div>

                 <div class="col-md-3">
					 <div class="contact-information">
						 <h3>Información de Contacto</h3>
						 <ul class="contact-information-list">
							 <li><span><i class="fa fa-home"></i> Av Acueducto 2900, Chapultepec Nte., 58240 Morelia, Mich.</span></li>
							 <li><a href="#"><i class="fa fa-envelope"></i></a></li>
                             <li><a href="#"><i class="fa fa-phone"></i> 443 324 7200</a></li>
                         </ul>
					 </div>
				 </div>

				 <div class="col-md-3">
					 <div class="contact-information">
						 <h3>Horario de Atención</h3>
						 <p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en Grupo FAME te escuchamos y atendemos de manera personalizada. </p>
						 <p class="work-time"><span>Lunes - Viernes</span> : 9:00 AM a 14:00 PM y 16:00 - 20:00 PM.</p>
						 <p class="work-time"><span>Sábado</span> : 9:00 AM a 17:00.</p>
                         <p class="work-time"><span>Domingo</span> <i>11:00 AM a 15:00 PM</i></p>
					 </div>
				 </div>

			 </div>
		 </div>
	 </div>
			
 </div>

 <?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>



 