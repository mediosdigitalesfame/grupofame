<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - BMW Talisman</title>

	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headertalisman.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>BMW - MINI</h2>
			</div>
		</div>
            
		 <div class="single-project-page">
			 <div class="container">
				 <div class="row">

					 <div class="col-md-12">
                            
                         <div class="col-md-4">
							 <div class="single-project-content">
                                  
                                 <br><br><br><h2>FAME Talismán</h2>
								 <p>
								 	 Te esperamos con gusto en: <br>
                                     <strong>Avenida Acueducto #2900, Colinia Lomas de Hidalgo <br>
                                     Morelia, Michoacán. C.P. 58240 <br>Teléfono: (443) 324 7200</strong>
                                 </p>
								 <p>
								 	 <a href="http://www.fametalisman.com/" target="_blank"><strong>www.fametalisman.com</strong></a>
								 </p>
                             </div>
                             <div class="col-md-4"> 
                             	 <img src="../../assets/images/s-agencias/bmw2.png">
                             </div>
                             <div class="col-lg-4">
                             	 <img src="../../assets/images/s-agencias/mini2.png">
                             </div>
                              
                         </div>
 
                         <div class="col-md-8">
	                         <div class="mfp-iframe-holder">

                                 <iframe width='100%' height='500px' frameBorder='0' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1878.1986282795322!2d-101.15771098278371!3d19.695704171250433!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d11ff61be2793%3A0xe275d09619564f01!2sFAME+BMW+MINI+Talism%C3%A1n+Morelia!5e0!3m2!1ses!2smx!4v1416934342543'></iframe>

                                 <small><a target="_blank" href="https://goo.gl/maps/jQw0V">Ver en Street View</a></small>
                             </div>
                         </div>

								
							</div>
						</div>

					</div>


				</div>
			</div>

		</div>
			</div><!-- End content -->
 <?php include('../../assets/contenido/agencias/footer.php'); ?>