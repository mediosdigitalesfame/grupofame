<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - KIA Motors</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headerkia.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>KIA Motors</h2>
			</div>
		</div>

		<!-- blog-box Banner -->
		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						
						
						<!-- -----------------------------------------------------------------------------------PEDREGAL-------------------------------------------------------------------------------- -->

						<div class="single-project-content">
							

							<div class="col-md-4" id="pedregal"><br><br><br><br>
								<h2>KIA Pedregal FAME</h2>
								<p>Te esperamos con gusto en: <br>
									<strong>Adolfo Ruíz Cortines No. 4000-Anexo  ·  Col. Jardines del Pedregal <br>
									Del. Álvaro Obregón  ·  C.P. 01900  ·  México, D.F.  ·  Teléfono: (55) 9136 3636</strong></p>
									<p><img src="../../assets/images/s-agencias/kia.png"></p>
									<p><a href="http://kiapedregal.com/" target="_blank"><strong>www.kiapedregal.com</strong></a></p>
								</div>
								<div class="col-md-8">
									<div class="mfp-iframe-holder">

										<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15061.655641155769!2d-99.2141634!3d19.3078374!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe1b9f36f0f29956f!2sKia+Pedregal+Fame!5e0!3m2!1ses-419!2smx!4v1465493513698" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe><br>

										<small><a href="https://www.google.com.mx/maps/place/KIA+Pedregal+FAME/@19.307877,-99.21558,3a,75y,30.87h,91.79t/data=!3m7!1e1!3m5!1s5kC-4Q9Q_78jtcqarYwXKw!2e0!6s%2F%2Fgeo3.ggpht.com%2Fcbk%3Fpanoid%3D5kC-4Q9Q_78jtcqarYwXKw%26output%3Dthumbnail%26cb_client%3Dmaps_sv.tactile.gps%26thumb%3D2%26w%3D100%26h%3D80%26yaw%3D40.952469%26pitch%3D0!7i13312!8i6656!4m2!3m1!1s0x85cdffbef0c6109f:0xe1b9f36f0f29956f!6m1!1e1?hl=es" target="_blank">Ver en Street View</a></small>

									</div></div>
 
								</div>
								<img src="../../assets/images/shadow.png" style="width:100%;">
								
								<!------------------------------------------------------------------------------------- MIL CUMBRES---------------------------------------------------------------------------------->
								<div class="single-project-content">

									<div class="col-md-4" id="mil">
										<br><br><br><br>	<h2>KIA Mil Cumbres FAME</h2>
										<p>Te esperamos con gusto en: <br>
											<strong>Av. Acueducto #3891 Col. Lomas de las Américas<br>
											C.P. 58254 · Morelia, Mich. Teléfono: 9136 3636</strong></p>
											<p><img src="../../assets/images/s-agencias/kia.png"></p>
											<p><a href="http://kiamilcumbres.com/" target="_blank"><strong>www.kiamilcumbres.com</strong></a></p>
										</div>  
										<div class="col-md-8">
											<div class="mfp-iframe-holder">

												<iframe width='100%' height='500px' frameBorder='0' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15026.226820526097!2d-101.138326!3d19.688909!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x384d46049af621e9!2sKIA+FAME+Morelia!5e0!3m2!1ses!2smx!4v1446840667131'></iframe><br>

												<small><a href="https://www.google.com.mx/maps/@19.6942772,-101.1473252,3a,75y,6.3h,86.66t/data=!3m6!1e1!3m4!1s8sIJxj26UKFTN1ovza6VjA!2e0!7i13312!8i6656!6m1!1e1" target="_blank">Ver en Street View</a></small>


											</div></div>
											
											<img src="../../assets/images/shadow.png" style="width:100%;">
											
											<!-------------------------------------------------------------------------------------PARICUTIN URUAPAN ---------------------------------------------------------------------------------->
											
											<div class="single-project-content">
												<div class="col-md-4" id="paricutin"><br><br><br>
													<h2>KIA Paricutín Uruapan</h2>
													<p>Te esperamos con gusto en: <br>
														<strong>Libramiento Oriente No.5849 Colonia Quirindabara.<br>
														C.P. 60190 · Uruapan, Mich. </strong></p><p><img src="../../assets/images/s-agencias/kia.png"></p>
														<p><a href="http://kiaparicutin.com/" target="_blank"><strong>www.kiaparicutin.com</strong></a></p>
													</div>  
													<div class="col-md-8">
														<div class="mfp-iframe-holder">

															<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3763.574043432511!2d-102.04339204886537!3d19.387588047076534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842de2eec32395f1%3A0xaada31ab134c9d03!2sKIA+Paricut%C3%ADn+Uruapan!5e0!3m2!1ses-419!2smx!4v1465492926882" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe><br>

														</div></div></div>

														
														<img src="../../assets/images/shadow.png" style="width:100%;">
														<!-------------------------------------------------------------------------------------DEL DUERO ZAMORA ---------------------------------------------------------------------------------->
														<div class="single-project-content">
															<div class="col-md-4" id="duero"><br><br><br>
																<h2>KIA del Duero Zamora</h2>
																<p>Te esperamos con gusto en: <br>
																	<strong> Madero sur #579 Col. Centro<br>
																	C.P. 59600 · Zamora, Mich. </strong></p><p><img src="../../assets/images/s-agencias/kia.png"></p>
																	<p><a href="http://kiadelduero.com/" target="_blank"><strong>www.kiadelduero.com</strong></a></p>
																</div>
																<div class="col-md-8">
																	<div class="mfp-iframe-holder">

																		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d937.4201704628084!2d-102.29019207082277!3d19.979926799160996!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842e88db31241cfb%3A0x6fc21cdadfec290!2sFrancisco+I.+Madero+Sur+LB%2C+La+Luneta+Oriente%2C+59670+Zamora%2C+Mich.!5e0!3m2!1ses-419!2smx!4v1465495181430" width="100%" height="500px" frameborder="0" style="border:0" allowfullscreen></iframe></div>


																	</div>
																</div> 
																
																<!-------------------------------------------------------------------------------------------- Fin de Mapa  ------------------------------------------------------------------------------>                          
																
															</div>

														</div>

													</div>
												</div>

											</div>
											
											<?php include('../../assets/contenido/agencias/footer.php'); ?>