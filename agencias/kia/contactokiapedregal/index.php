<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - KIA Pedregal</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerkiacontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - KIA Pedregal</h2>
			</div>
		</div>
		
		<br>

		<div class="contact-box">
			<div class="container">
				<div class="row">

					<div class="col-md-6" align="center">

						<div class="container">
								<div class="col-md-12" >
									<a href="https://api.whatsapp.com/send?phone=524431810975&text=Hola,%20Quiero%20más%20información!" target="_blank" title="WhatsApp">
										<button type="button" class="btn btn-success"><i class="fa fa-whatsapp fa-3x">
										</i> <font size="6"> WhatsApp</font> 
									</button>
								</a>
							</div>
						</div>

						<br>


						<div class="container">
							<div class="col-md-12" >
								<?php include('../../../assets/formularios/formkiapedre.php'); ?>

							</div>
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h2>KIA Pedregal FAME</h2>
							<br>
							<h3>Información de Contacto</h3>
							<ul class="contact-information-list">
								<li><span><i class="fa fa-home"></i>Blvd. Adolfo Ruíz Cortínes No. 4000 BIS Col. Jardines del Pedregal, Del. Álvaro Obregón México. Ciudad de México C.P. 01900</span></li>
								<li><a href="#"><i class="fa fa-phone"></i>(55) 9136-3636</a></li>
								 

							</ul>

  									<p><img src="../../../assets/images/s-agencias/kia.png"></p>
									<p><a href="http://kiapedregal.com/" target="_blank"><strong>www.kiapedregal.com</strong></a></p>
 
						</div>
					</div>

					<div class="col-md-3">
						<div class="contact-information">
							<h3>Horario de Atención</h3>
							<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en Grupo FAME te escuchamos y atendemos de manera personalizada. </p>
							<h4>Ventas</h4>
							<p class="work-time"><span>Lunes - Viernes</span> : 9:00 AM a 7:00 PM</p>
							<p class="work-time"><span>Sábado</span> : 9:00 AM - 5:00 PM.</p>
							<p class="work-time"><span>Domingo</span> <i>Cerrado</i></p>
							<h4>Servicio</h4>
							<p class="work-time"><span>Lunes - Viernes</span> : 8:00 AM a 7:00 PM</p>
							<p class="work-time"><span>Sábado</span> : 9:00 AM - 2:00 PM.</p>
							<p class="work-time"><span>Domingo</span> <i>Cerrado</i></p>
							<h4>Refacciones</h4>
							<p class="work-time"><span>Lunes - Viernes</span> : 9:00 AM a 7:00 PM</p>
							<p class="work-time"><span>Sábado</span> : 9:00 AM - 2:00 PM.</p>
							<p class="work-time"><span>Domingo</span> <i>Cerrado</i></p>
							<h4>Caja</h4>
							<p class="work-time"><span>Lunes - Viernes</span> : 9:00 AM a 7:00 PM</p>
							<p class="work-time"><span>Sábado</span> : 9:00 AM - 2:00 PM.</p>
							<p class="work-time"><span>Domingo</span> <i>Cerrado</i></p>
						</div>
					</div>

				</div>
			</div>
		</div>
		
	</div>


	<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>