<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - KIA Mil Cumbres</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerkiacontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - KIA Mil Cumbres</h2>
			</div>
		</div>
		
		<div class="contact-box">
			<div class="container">
				<div class="row">

					<div class="col-md-6" align="center">
						<div class="container">
							<div class="col-md-12" >
								<?php include('jotform.php'); ?>
							</div>
						</div>

					</div>

				<div class="col-md-6">
					 <div class="contact-information">
					 <object data="Ficha_Tecnica_RIO_HB.pdf" width="100%" height="721"> </object>  
					 </div>
				 </div>
			 </div>

				</div>
			</div>
		</div>
		
	</div>


	<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>