<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - VolksWagen</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headervw.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>VolksWagen</h2>
			</div>
		</div>

		<!-- blog-box Banner -->
		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						<div class="single-project-content">
							<!--								<img alt="" src="upload/single-project.jpg"> -->

							<div class="col-md-4"><br><br><br><h2>FAME Volkswagen Autos</h2>
								<p>Te esperamos con gusto en: <br>
									<strong>Av. Acueducto No. 2823 Col. Lomas de Hidalgo <br>
										Morelia, Michoacán, C.P. 58240.<br> Teléfono: (443) 314-00-50</strong></p><img src="../../assets/images/s-agencias/vw.png"></p>
										<p><a href="http://www.vw-fame.com.mx/" target="_blank"><strong>www.vw-fame.com.mx</strong></a></p>
									</div>
									<div class="col-md-8">
										<div class="mfp-iframe-holder">

											<iframe width='100%' height='500px' frameBorder='0' src='https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d1878.198752178108!2d-101.1588319851807!3d19.695693612701366!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x842d0df84b25f11f%3A0xe285e9731525fc10!2sVolkswagen+Autos+Fame!5e0!3m2!1ses-419!2smx!4v1417039894892'></iframe>

											<small><a target="_blank" href="https://goo.gl/maps/DnZop" style="color:#848484;text-align:left">Ver en Street View</a></small>
										</div></div>
									</div>
								</div>
							</div>
						</div>
					</div>

				</div>
				<!-- End content -->

				<?php include('../../assets/contenido/agencias/footer.php'); ?>