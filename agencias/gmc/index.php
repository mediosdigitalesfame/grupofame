<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - GMC, Buick y Cadillac</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headergmc.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2></h2>
			</div>
		</div>
 
	 <div class="single-project-page">
		 <div class="container">
			 <div class="row">

				 <div class="col-md-12">
 
					 <div class="single-project-content">
						 <div class="mfp-iframe-holder">

						 	 <div class="col-md-4">
                                 <h2>FAME Camelinas - GMC, Buick y Cadillac</h2>
							     <p>
							 	     Te esperamos con gusto en: <br>
                                     <strong>Periférico Paseo de la República #2655<br> Colonia Las Camelinas,
                                     Morelia, Michoacán. <br>C.P. 58186<br> Teléfono: (443) 334 4440</strong>
                                 </p>
							     <p>
							 	     <a href="http://www.famemanantiales.com/" target="_blank"><strong>www.famemanantiales.com</strong></a>
							     </p>
							     <div class="col-md-4"><img src="../../assets/images/s-agencias/cadillac.png"></div>
                                 <div class="col-md-4"><img src="../../assets/images/s-agencias/gmc.png"></div>
                                 <div class="col-md-4"><img src="../../assets/images/s-agencias/buick.png"></div>
						     </div>
           
                             <div class="col-md-8">
                                 <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15026.71146568071!2d-101.1671!3d19.683744!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xd6028454e5bb7cc1!2sGMC%2C+Buick+y+Cadillac+Morelia!5e0!3m2!1ses-419!2smx!4v1485299603714" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>

                                 <small><a target="_blank" href="https://goo.gl/maps/mduMI">Ver en Street View</a></small>
                             </div>

                         </div>
			         </div>
		         </div>
             </div>
         </div>
     </div>
 </div>

<?php include('../../assets/contenido/agencias/footer.php'); ?>
			 