	<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - GMC Uruapan</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">


		<div id="content">

            <?php include('../../../assets/contenido/agencias/headergmccontacto.php'); ?>
            
			<div class="page-banner">
				<div class="container">
					<h2>Contacto - GMC, Buick Y Cadillac Morelia </h2>

				</div>
			</div>
			<!-- contact box -->
			<div class="contact-box">
				<div class="container">
					<div class="row">
                            
                            <div class="col-md-6" align="center">
                                 <div class="container">
					                 <div class="col-md-12" >
								          <?php include('../../../assets/formularios/formgmcuru.php'); ?>
                                     </div>
                                 </div>
                            </div>
                            
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Prolongación Paseo Lázaro Cárdenas #3591</span> <span>Fracc. Jardines del Bosque C.P. 60190 </span> <span>Uruapan, Michoacán</span></li>
									<li><span><i class="fa fa-phone"></i><strong>(452) 503 3907</strong></span></li>
       
		</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>FAME Manantiales</strong>; te escuchamos y atendemos de manera personalizada. </p>
                                <h4>Ventas</h4>
								<p class="work-time"><span>Lunes - Viernes:</span>  09:00 - 19:30 hrs.</p>
								<p class="work-time"><span>Sábado:</span>  09:00 - 17:00 hrs.</p>
                                <p class="work-time"><span>Domingo:</span> 11:00 - 15:00 hrs.</p><br>

                                <h4>Servicio y Administración</h4>
								<p class="work-time"><span>Lunes - Viernes:</span>  09:00 - 19:00 hrs.</p>
								<p class="work-time"><span>Sábado:</span>  09:00 - 14:00 hrs.</p>
							</div>
						</div>
							
 
					</div>
				</div>
			</div>

		</div> 

 <?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>