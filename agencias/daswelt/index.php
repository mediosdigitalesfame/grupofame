<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - DasWelt</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headerdaswelt.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>DasWelt</h2>
			</div>
		</div>

		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						<div class="col-md-4">
							<div class="single-project-content">
								<!--								<img alt="" src="upload/single-project.jpg"> -->
								<br><br><br><h2>Das WeltAuto Morelia <br> </h2> <h3>Seminuevos garantizados</h3><br>

								<p><strong> Te esperamos con gusto en: </strong><br>
									Blvrd Juan Pablo II 2480, Jesús del Monte, 58350 Morelia, Mich.<br><br>
									<span><i class="fa fa-phone"></i></span> <strong>Teléfono: (443) 204 0544</strong>
								</p>
								<p>
									<!--<a href="http://www.audicenterqueretaro.com.mx/AC_Home.asp?idc=2150&idS=1" target="_blank"><strong>www.fiatqueretarofcamx.cms.dealer.com</strong></a>-->

									<div class="col-md-10"><img src="../../assets/images/s-agencias/daswelt.png"></div>

								</p>
							</div></div>

							<div class="col-md-8">
								<div class="mfp-iframe-holder">

									<iframe width='100%' height='500px' frameBorder='0'  src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15028.385436552237!2d-101.165816!3d19.665894!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xb579e765a0d07f25!2sDas+WeltAuto+Seminuevos+Fame+Altozano+Morelia!5e0!3m2!1ses-419!2smx!4v1481051624149"></iframe>

									<small><a target="_blank" href="https://goo.gl/maps/LbB82cbUegy" style="color:#848484;text-align:left">Ver en Street View</a></small>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- End content -->

	<?php include('../../assets/contenido/agencias/footer.php'); ?>