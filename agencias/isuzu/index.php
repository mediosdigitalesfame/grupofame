<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Isuzu Camiones</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">
		
		<?php include('../../assets/contenido/agencias/headerisuzu.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Isuzu Camiones</h2>
			</div>
		</div>

		<!-- blog-box Banner -->
		<div class="single-project-page">
			<div class="container">
				<div class="row">

					<div class="col-md-12">
						<div class="single-project-content">
							<!--								<img alt="" src="upload/single-project.jpg"> -->
							<div class="col-md-4"><br><br><br><h2>Isuzu FAME Camiones</h2>
								<p>Te esperamos con gusto en: <br>
									<strong>Periférico Paseo de la República No. 511  Colonia Los Manantiales <br>
										Morelia, Mich. CP 58170 <br>Teléfono: (443) 298 1944</strong></p>
										<p><img src="../../assets/images/s-agencias/isuzu.png"></p>
										<p><a href="http://www.famecamiones.com" target="_blank"><strong>www.famecamiones.com</strong></a></p>
									</div>
									<div class="col-md-8">
										<div class="mfp-iframe-holder">

											<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3756.4891796740785!2d-101.24142768508923!3d19.691786986737636!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc5ffbcf4f435a5ef!2sFAME+Isuzu!5e0!3m2!1ses-419!2smx!4v1538763355672" width="100%" height="500" frameborder="0" style="border:0" allowfullscreen></iframe>

											<small><a target="_blank" href="https://goo.gl/maps/5txqw" style="color:#848484;text-align:left">Ver en Street View</a></small>
										</div></div>


									</div>
								</div>

							</div>


						</div>
					</div>

				</div>
				<!-- End content -->

				<?php include('../../assets/contenido/agencias/footer.php'); ?>