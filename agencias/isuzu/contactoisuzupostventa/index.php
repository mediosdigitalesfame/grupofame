<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Isuzu Camiones</title>
	<?php include('../contenido/headcontacto.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../../../assets/contenido/agencias/headerisuzucontacto.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Contacto - Isuzu Camiones</h2>
			</div>
		</div>

			<div class="contact-box">
				<div class="container">
					<div class="row">
                        
                        <div class="col-md-6" align="center">
                         <div class="container">
					         <div class="col-md-12" >
								 <?php include('../../../assets/formularios/formizusupostventa.php'); ?>
                             </div>
                         </div>
                     </div>
                        
						<div class="col-md-3">
							<div class="contact-information">
								<h3>Información de Contacto</h3>
								<ul class="contact-information-list">
									<li><span><i class="fa fa-home"></i>Periférico Paseo de la República #511</span> <span>Col. Los Manantiales </span> <span> Morelia, Michoacán</span> <span>C.P. 58170</span></li>
									<li><span><i class="fa fa-phone"></i><strong>(443) 298 1944</strong></span></li>
                                    <li><i class="fa fa-phone"></i><span>Servicio <strong>2981944/45</strong></span>.<br>
                                   	<i class="fa fa-phone"></i><span>Refacciones <strong>298 1944 y 298 1945</strong></span>.<br>
                                    <i class="fa fa-phone"></i><span>Postventa<strong> </strong></span><strong> 2981944/45.</strong><br>
                                    <i class="fa fa-phone"></i><span>Ventas <strong>2981944 y 298 1945.</strong></span><br>                
                                    <i class="fa fa-phone"></i><span>Recepción <strong>2981944 y 298 1945</strong></span>.<br>
                                    </li>
                                    <h3>Whatsapp</h3>
                       				<li><span><i class="fa fa-whatsapp"></i><strong>   Ventas | 4434101231</strong></span></li>
<!--									<li><a href="#"><i class="fa fa-envelope"></i>contacto@hondacamelinas.com</a></li>-->
								</ul>
							</div>
						</div>

						<div class="col-md-3">
							<div class="contact-information">
								<h3>Horario de Atención</h3>
								<p>Con gusto esperamos tu llamada en nuestro <strong>Call Center</strong>, para cualquier duda, aclaración o sugerencia que quieras compartirnos en <strong>Isuzu FAME Camiones</strong>; te escuchamos y atendemos de manera personalizada. </p>
								<p class="work-time"><span>Lunes - Viernes</span> : 9:00 a.m. - 7:00 p.m.</p>
								<p class="work-time"><span>Sábado</span> : 9:00 a.m. - 2:00 p.m.</p>
							</div>
						</div>



 </div>
			 </div>
		 </div>

		</div> 

<?php include('../../../assets/contenido/agencias/footercontacto.php'); ?>
