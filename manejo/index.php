<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Cita de Manejo</title>
	<?php include('contenido/head.php'); ?>
</head>    
<body>
	<div id="container">

		<?php include('../assets/contenido/header/headermanejo.php'); ?>

		<div class="page-banner">
			<div class="container">
				<h2>Agenda Tu Cita de Manejo</h2>
			</div>
		</div>
 
	 <div class="contact-box">
		 <div class="container">
			 <div class="row">

				 <div class="col-md-3">
				 </div>

				 <div class="col-md-6" align="center">
                     <div class="container">
					     <div class="col-md-12" >
							 <?php include('../assets/formularios/formmanejo.php'); ?> 	
                         </div>
                     </div>
                 </div>
 
			 </div>
		 </div>
	 </div>
			
 </div>

<?php include_once('../assets/contenido/footer.php'); ?>