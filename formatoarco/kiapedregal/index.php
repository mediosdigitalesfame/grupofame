<!doctype html>
<html lang="es" class="no-js">
<head>

	<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-91159946-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-91159946-1');
</script>


	<title>Grupo FAME - Formato ARCO - KIA Pedregal</title>
	<?php include('../../assets/contenido/headarcoagencia.php'); ?>
</head>    
<body>
	<div id="container">
		<?php include('../../assets/contenido/header/headerarcoagencia.php'); ?>   
		<div class="page-banner">         
			<div class="container">
				<h2>Formato ARCO KIA Pedregal</h2>
			</div>
		</div>

	  

		<div class="section">
			<div align="center" id="about-section">
				 
				<embed src="../../assets/pdfs/FORMATO ARCO FAME MOTORS S.A. DE C.V.pdf" type="application/pdf" width="60%" height="600px" />
				 
			</div>
		</div>

	</div>
</body>
<?php include_once('../../assets/contenido/footeragenciaarco.php'); ?>